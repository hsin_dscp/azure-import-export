# Load the CommanFunctions.ps1 script
. "$PSScriptRoot\CommanFunctions.ps1"
# Load the ImportGlobalConfigurations.ps1 script
. "$PSScriptRoot\ImportGlobalConfigurations.ps1"
# Load the DevOpsRestApiOperations.ps1 script
. "$PSScriptRoot\DevOpsRestApiOperations.ps1"
# Load the ImportRollback.ps1 script
. "$PSScriptRoot\ImportRollback.ps1"

# Function to handle import of Queries
function ImportQueries {
  
    # Checks for the presence of the named query in the project at the destination organization
    function TestForPresenceAtTarget {
        param(
            [string]$uri
        )
        try { 
            $results = Invoke-RestMethod -Uri $uri -Headers @{Authorization = "Basic $global:ApiKey" } 
        }
        catch {}
        return ($null -ne $results) 
    }    
    # replaces source organization url references in queries with the destination organization url
    function CleanProjectReferences {
        param (
            [Parameter(ValueFromPipeline)] 
            $InputObject
        )
    
        process {
            if ($null -eq $InputObject) { return $null }
    
            if ($InputObject -is [System.Linq.Enumerable] -and $InputObject -isnot [string]) {
                $collection = @(
                    foreach ($object in $InputObject) { CleanProjectReferences $object }
                )
    
                Write-Output -NoEnumerate $collection
            }
            elseif ($InputObject -is [psobject]) {
                $hash = @{}
    
                foreach ($property in $InputObject.PSObject.Properties) {
                    $hash[$property.Name] = CleanProjectReferences $property.Value
                }
    
                $hash
            }
            else {
                if ($InputObject -is [string]) {
                    $InputObject.Replace($sourceOrganization, $sourceOrganizationName)
                }
                else {
                    $InputObject
                }
            }
        }
    }
    # Creates the query in the project at the destination
    function CreateQueryInTarget {
        param(
            [psobject]$queryObject,
            [string]$createUrl
                
        )
        #remove properties which will be assigned at destination
        $queryObject.PSObject.properties.remove('id')
        $queryObject.PSObject.properties.remove('_links')
        $queryObject.PSObject.properties.remove('url')
        $queryObject.PSObject.properties.remove('linkClauses')
        $queryObject.PSObject.properties.remove('targetClauses')
        
        #update internal references with the target environemnt
        $replaced = CleanProjectReferences -InputObject $queryObject 

        # Check if the "sortColumns" property exists before removing it
        if ($replaced.ContainsKey('sortColumns')) {
            $replaced.Remove("sortColumns")
        }    
       
        # $replaced = ConvertTo-Json $replaced -Depth 10 
        $response = $null
        $response = PostContent -uri $createUrl -requestBody $replaced
        $response | ConvertTo-Json | Out-File $global:QueryResponsePath -Append
        return $response
    }    
    # Runs through queries and folders in the source, copying them to the destination 
    function ProcessQueryFolder() {
        param(
            [psobject]$children
        )

        $children | ForEach-Object {
            #process queries in current folder
            $targetUri = $global:CreateQueryUri -f $projectName, $_.path  
            #queries and folders need to be posted to the parent
            $parentFolderUri = "$($targetUri.Substring(0, $targetUri.LastIndexOf('/')))?api-version=7.0"
            #only used for tracing
            $parentPath = $_.path.Substring(0, $_.path.LastIndexOf('/'))
    
            if ($_.isFolder -ne $true) {
                #test for presence of query in target
                if (TestForPresenceAtTarget -uri $targetUri ) {
                    #skip if found
                    Write-Host " [Skip Query] '$($_.name)' found at : $($_.path)" -ForegroundColor Gray
                }
                else {
                    #otherwise create the query in its parent folder
                    $queryInTarget = CreateQueryInTarget -queryObject $_ -createUrl $parentFolderUri 
                    LogMessage -type "Info" -message $(" [Query]  $($queryInTarget.name)' is created under '$($parentPath)") -errorTrace $null -writeHost $true
                }
            }
            else {
                #test for presence of folder in target
                if (TestForPresenceAtTarget -query $targetUri ) {
                    #skip if found
                    Write-Host " [Skip Folder] '$($_.name)' found at : $($_.path)"  -ForegroundColor Gray
                }
                else {
                    #otherwise create the folder under its parent folder
                    $folder = @{
                        name     = "$($_.name)" 
                        isFolder = $true
                    } 

                    $folderResponse = PostContent -uri $parentFolderUri -requestBody $folder 
                    LogMessage -type "Info" -message $(" [Folder]  $($folderResponse.name)' is created under '$($parentPath)") -errorTrace $null -writeHost $true
                }
                #recursively call the method to create children folders and queries of the new folder
                if ($null -ne $_.Children) {
                    ProcessQueryFolder -children $_.Children
                }
            }
        }       
    }
    
    if ((Test-Path $global:ProjectResponsePath -PathType Leaf)) {
        $projects = Get-Content $global:ProjectResponsePath -Encoding UTF8 | ConvertFrom-Json

        foreach ($project in $projects) {
            $queries_path = $global:QueryFilePath -f $project.oldProjectId 
            $queries = Get-Content $queries_path -Encoding UTF8 | ConvertFrom-Json 
            $projectName = $project.name
            $queries.value | ForEach-Object {
                if ($_.Children.Count -gt 0) {
                    ProcessQueryFolder -children $_.Children 
                }
            }
        }

        $inputFilePath = $global:QueryResponsePath 
        if ((Test-Path $inputFilePath -PathType Leaf)) {
            # Read the content of the file as a string
            $fileContent = Get-Content -Path $inputFilePath -Raw
            # Replace }{ with },{ in the file content
            $modifiedContent = $fileContent -replace '}\r?\n{', '},{'
            # Add [ at the start and ] at the end
            $finalContent = "[${modifiedContent}]"
            # Write query response on path
            $finalContent  |  Out-File $inputFilePath 
        }
    }     
}

# Function to handle import of Dashboard with Widgets
function ImportDashboardWithWidgets {
   
    if ((Test-Path $global:ProjectResponsePath -PathType Leaf)) {
        $projects = Get-Content $global:ProjectResponsePath -Encoding UTF8 | ConvertFrom-Json
        $projects | ForEach-Object {
            $projectName = $_.name
            $dashboard_path = $global:GetDashboardsFilePath -f $_.oldProjectId 
            $dashboards = Get-Content $dashboard_path -Encoding UTF8 | ConvertFrom-Json 
            $dashboardUrl = $global:CreateDashboardUrl -f $projectName 
            $team = GetTeam $projectName
            $teamDashboardUrl = $global:CreateTeamDashboardUrl -f $projectName, $team.id
            
            foreach ($dashboard in $dashboards.value) {
                $widgetsPath = $global:GetDashboardWidgetsFilePath -f $dashboard.id 
                $widget = Get-Content $widgetsPath -Encoding UTF8 | ConvertFrom-Json 
                if ($null -ne $widget) {
                    if ($dashboard.name -ne "Overview") {
                
                        $param = @{
                            "name"            = $widget.name;
                            "position"        = $widget.position;
                            "description"     = $widget.description;
                            "eTag"            = $widget.eTag;
                            "groupId"         = $team.id;
                            "ownerId"         = $team.id;
                            "dashboardScope"  = $widget.dashboardScope;
                            "refreshInterval" = $widget.refreshInterval;
                            "widgets"         = @(GetWidgets $widget.widgets $_)              
                        }
                        if($widget.dashboardScope -eq "project_Team")
                        {
                            $url = $teamDashboardUrl
                        }
                        else{
                            $url = $dashboardUrl
                        }
                        $response = PostContent -uri $url -requestBody $param 
                        if ($null -ne $response) {
                            LogMessage -type "Info" -message $(" [Dashboard] " + $response.name + " is imported successfully.") -errorTrace $null -writeHost $true
                        }
                    }
                    else {
                        $existingDashboards = CheckIsPresenceInTarget $dashboardUrl
                        if ($null -ne $existingDashboards ) {
                            $existingDashboard = $existingDashboards.value | Where-Object { $_.name -eq $dashboard.name }
                            $updateDashboardUrl = $($global:UpdateDashboardUri -f $_.newProjectId, $existingDashboard.groupId, $existingDashboard.id)
                   
                            $modifiedWidget = @{
                                "name"            = $existingDashboard.name;
                                "dashboardScope"  = $existingDashboard.dashboardScope;
                                "description"     = $existingDashboard.description;
                                "eTag"            = $existingDashboard.eTag;
                                "groupId"         = $existingDashboard.groupId;
                                "ownerId"         = $existingDashboard.ownerId;
                                "id"              = $existingDashboard.id;
                                "position"        = $existingDashboard.position;
                                "refreshInterval" = $existingDashboard.refreshInterval;
                                "widgets"         = @(GetWidgets $widget.widgets $_)
                            }
                            $response = PutContent -uri $updateDashboardUrl -requestBody $modifiedWidget
                            if ($null -ne $response) {
                                LogMessage -type "Info" -message $(" [Dashboard] " + $response.name + " widgets are imported successfully.") -errorTrace $null -writeHost $true
                            }
                        }
                    }
                }
            }
        }
    }
}


# Function to get Widgets from exported data
function GetWidgets {
    param (
        [PSCustomObject]$widgets,
        [PSCustomObject]$project
    )
    
    $projectId = $project.newProjectId
    $oldProjectId = $project.oldProjectId
    $projectName = $project.name
    $team = GetTeam $projectName
    
    $exportWidgets = $widgets | ForEach-Object {
        # Remove unnecessary properties
        $_.PSObject.Properties.Remove('loadingImageUrl')
        $_.PSObject.Properties.Remove('url')
        $_.PSObject.Properties.Remove('id')
        $_.PSObject.Properties.Remove('eTag')
        $_.PSObject.Properties.Remove('_links')
        
        if ($null -ne $_.settings) {
            $settings = $_.settings | ConvertFrom-Json

            if ($settings.PSObject.Properties["queryName"] -or ($null -ne $settings.query -and $settings.query.queryName)) {
                if ($null -ne $settings.queryId) {
                    $settings.queryId = GetExportedQueryName $settings.queryId $oldProjectId
                }
                elseif ($null -ne $settings.query.queryId) {
                    $settings.query.queryId = GetExportedQueryName $settings.query.queryId $oldProjectId
                }
            }
            elseif ($settings.PSObject.Properties["teams"]) {
                $settings.teams | ForEach-Object {
                    if ($null -ne $_.PSObject.Properties["teamId"]) {
                        $_.teamId = $team.id
                    }
                }
                $settings.teams | ForEach-Object {
                    $_.projectId = GetNewProjectId $projectName
                    $_.teamId = $team.id
                }
                if($settings.PSObject.Properties["projectId"]) {
                    
                }
            }
            elseif ($settings.PSObject.Properties["dataSettings"]) {
                $settings.dataSettings | ForEach-Object {
                    if ($settings.dataSettings.PSObject.Properties["project"]) {
                        $settings.dataSettings.project = $projectId
                    }
                    if ($null -ne $_.PSObject.Properties["teamIds"]) {
                        $teamIds = @()
                        $settings.dataSettings.teamIds | ForEach-Object {
                            $teamIds += $team.id
                        }
                    }
                    if ($teamIds.Count -gt 0) {
                        $settings.dataSettings.teamIds = $teamIds
                    }
                }
            }
            elseif ($settings.PSObject.Properties["repo"]) {
                $repository = GetNewRepository $projectName
                $settings.repo.id = $repository.id                
                if ($settings.repo.PSObject.Properties["project"]) {
                    $project = $settings.repo.project
                    $project.id = $projectId
                    $settings.repo.project = $project
                }
            }
            elseif ($settings.PSObject.Properties["scope"]) {
                if ($settings.PSObject.Properties["groupKey"]) {
                    $queryId = GetExportedQueryName $settings.groupKey $oldProjectId
                    $settings.groupKey = $queryId
                    $settings.transformOptions.filter = $queryId
                }
            }

            if ($settings.PSObject.Properties["teamId"]) {
                $settings.teamId = $team.id
            }

            $_.settings = $settings | ConvertTo-Json -Depth 50
        }
        $_
    }    
    return $exportWidgets
}

# Function to get newlly created projectId
function GetNewProjectId {
    param (
        [string] $projectName
    )
    if ((Test-Path $global:ProjectResponsePath -PathType Leaf)) {
        $projects = Get-Content $global:ProjectResponsePath -Encoding UTF8 | ConvertFrom-Json
        $project = $projects | Where-Object {$_.name -eq $projectName}
        $project.newProjectId
    }
}

# Function to handle get newlly generated teamId
function GetTeam {
    param (
        [string] $projectName
    )
    $teams = GetContent $global:GetTeamsUri
    return $teams.value | Where-Object { $_.projectName -eq $projectName }
}

# Function to handle get newlly created queryId
function GetNewQueryId {
    param (
        [string] $queryName
    )
    
    $inputFilePath = $global:QueryResponsePath 
    if ($queryName -ne "" -and (Test-Path $inputFilePath -PathType Leaf)) {
        # Read the content of the file as a string
        $fileContent = Get-Content -Path $inputFilePath -Encoding UTF8 | ConvertFrom-Json
        $query = $fileContent | Where-Object { $_.name -eq $queryName }
        $queryId = $query.id 
    }

    return $queryId
}

# Function to get exported QueryName to Get newlly created repository details to associate it with widgets
function GetExportedQueryName {
    param (
        [string]$queryId,
        [string]$oldProjectId
    )

    $queriesPath = $global:QueryFilePath -f $oldProjectId 
    $queries = Get-Content $queriesPath -Encoding UTF8 | ConvertFrom-Json 
    $query = FindQuerieById $queries.value $queryId 
    if ($null -ne $query) {
        return GetNewQueryId $query.name
    } 
    return $queryId
}

# Function to find an object by ID in a nested list of objects with dynamic depth
function FindQuerieById ($objects, $targetId) {
    foreach ($object in $objects) {
        if ($object.id -eq $targetId) {
            return $object  # Object found, return it
        } 
        # If the current object has children, recursively search through them
        if ($null -ne $object.children -and $object.children.Count -gt 0) {
            $foundObject = FindQuerieById $object.children $targetId
            if ($null -ne $foundObject) {
                return $foundObject  # Object found in children, return it
            }
        }
    } 
    return $null  # Object with the specified ID not found
}


# Function to Get newlly created repository details to associate it with widgets
function GetNewRepository {
    param (
        [string]$projectName
    )
    $repositories = GetContent -uri $($global:GetRepositoryUri -f $projectName)
    return $repositories.value | Where-Object { $_.name -eq $projectName }
}
# Function to handle import of WorkItemType layout
function ImportWorkItemTypeLayout {
    param (
        [PSCustomObject]$wortItemType,
        [string] $oldProcessId,
        [string] $newProcessId,
        [string] $newReferenceName
    )
    # Import layout to target organization

    $referenceName = $wortItemType.referenceName 

    # Import Work item type fields to target organization
    ImportWorkItemTypeFields $wortItemType $oldProcessId $newProcessId $newReferenceName
     
    $layoutfilepath = $global:WorkItemTypeLayoutFilePath -f $oldProcessId , $referenceName 
    if ((Test-Path $layoutfilepath -PathType Leaf)) {
        $layout = Get-Content $layoutfilepath -Encoding UTF8 | ConvertFrom-Json
        foreach ($page in $layout.pages) {
            $pageResponse = ImportWorkItemTypePage $page $wortItemType $newProcessId $newReferenceName            
            $pageId = GetIdFromResponse $pageResponse $page
            foreach ($section in $page.sections) {
                $sectionId = $section.id
                foreach ($group in $section.groups) {
                    $groupResponse = ImportWorkItemTypeGroup $group $newReferenceName $newProcessId $pageId $sectionId
                    $groupId = $groupResponse.id # GetIdFromResponse $groupResponse $group                        
                    foreach ($control in $group.controls) {
                        if ($control.inherited -ne $true) {
                            ImportWorkItemTypeControl $control $newReferenceName $newProcessId $pageId $groupId $sectionId
                        }
                        elseif ($control.overridden -eq $true) {
                            UpdateWorkItemTypeControl $control $newReferenceName $newProcessId $pageId $groupId $sectionId
                        } 
                    }
                }
            }
        }
    }   

    # Import Work item type states to target organization
    ImportWorkItemTypeStates $wortItemType $oldProcessId $newProcessId $newReferenceName

    # Import Work item type rules to target organization
    ImportWorkItemTypeRules $wortItemType $oldProcessId $newProcessId $newReferenceName
}

# Function to handle import of WorkItemType States
function ImportWorkItemTypeStates {
    param (
        [PSCustomObject] $wortItemType,
        [string] $oldProcessId,
        [string] $newProcessId,
        [string] $newReferenceName
    )
    $referenceName = $wortItemType.referenceName 
    $statesFilePath = $global:WorkItemTypeStatesFilePath -f $oldProcessId, $referenceName 
    $stateUri = $global:WorkItemTypeStatesUri -f $newProcessId, $newReferenceName
    $existingStates = CheckIsPresenceInTarget $stateUri
    if ( $null -eq $existingStates) {
        $stateUri = $global:WorkItemTypeStatesUri -f $newProcessId, $wortItemType.inherits
        $existingStates = GetContent $stateUri
    }

    if ((Test-Path $statesFilePath -PathType Leaf)) {

        $states = Get-Content $statesFilePath -Encoding UTF8 | ConvertFrom-Json

        foreach ($state in $states.value | Where-Object { $_.customizationType -eq "custom" }) { 
            #skip if found
            if ($existingStates.value | Where-Object { $_.name -eq $state.name }) {
                Write-Host " [State] $($state.name) already present" -ForegroundColor Gray
            }
            else {
                $param = @{
                    "name"          = $state.name;
                    "color"         = $state.color;
                    "stateCategory" = $state.stateCategory
                }
                $response = PostContent -uri $stateUri -requestBody $param
                if ($null -ne $response) {
                    LogMessage -type "Info" -message $(" [State] " + $response.name + " is imported successfully.") -errorTrace $null -writeHost $true
                } 
            }
        }
    }
}

# Function to handle import of WorkItemType Rules
function ImportWorkItemTypeRules {
    param (
        [PSCustomObject] $wortItemType,
        [string] $oldProcessId,
        [string] $newProcessId,
        [string] $newReferenceName
    )

    $referenceName = $wortItemType.referenceName 
    $rulesFilePath = $global:WorkItemTypeRulesFilePath -f $oldProcessId, $referenceName 
    
    if ((Test-Path $rulesFilePath -PathType Leaf)) {

        $rules = Get-Content $rulesFilePath -Encoding UTF8 | ConvertFrom-Json
        $ruleUri = $global:WorkItemTypeRulesUri -f $newProcessId, $newReferenceName 
        $existingRules = CheckIsPresenceInTarget $ruleUri
        if ($null -eq $existingRules ) {
            $ruleUri = $global:WorkItemTypeRulesUri -f $newProcessId, $wortItemType.inherits
            $existingRules = GetContent $ruleUri
        }

        foreach ($rule in $rules.value | Where-Object { $_.customizationType -eq "custom" }) {
            #skip if found
            if ($existingRules.value | Where-Object { $_.name -eq $rule.name }) {
                Write-Host " [Rule] $($rule.name) already present" -ForegroundColor Gray
            }
            else {
                $param = @{
                    "name"       = $rule.name
                    "conditions" = @(GetRulesConditions $rule.conditions)  
                    "actions"    = @(GetRulesActions $rule.actions) 
                    "isDisabled" = $rule.isDisabled
                }
                $response = PostContent -uri $ruleUri -requestBody $param       
                if ($null -ne $response) {
                    LogMessage -type "Info" -message $(" [Rule] " + $response.name + " is imported successfully.") -errorTrace $null -writeHost $true
                }             
            }
        }
    }
}

# Function to get the all conditions for each rule
function GetRulesConditions {
    param (
        [PSCustomObject]$conditions
    )
    $jsonArray = foreach ($condition in $conditions) {
        @{
            "conditionType" = $condition.conditionType
            "field"         = $condition.field
            "value"         = $condition.value
        }
    }
    # No need to convert to JSON here, return the PowerShell array
    return $jsonArray
}

# Function to prepage actions
function GetRulesActions {
    param (
        [PSCustomObject]$actions
    )
    $jsonArray = foreach ($action in $actions) {
        @{
            "actionType"  = $action.actionType
            "targetField" = $action.targetField
            "value"       = $action.value
        }
    }

    # No need to convert to JSON here, return the PowerShell array
    return $jsonArray
}

# Function to handle get Existing Field
function GetExistingField {
    param (
        [string] $referenceName,
        [string] $name
    )
   
    $existingFields = CheckIsPresenceInTarget $global:ProcessFieldsUri 
    $existingField = $existingFields.value | Where-Object { $_.referenceName -eq $referenceName }
    if($null -eq $existingField){
        $existingField = $existingFields.value | Where-Object { $_.name -eq $name }
    }
    return $existingField
}

# Function to handle get new referenceName
function GetNewFieldReferenceName {
    param (
        [string] $referenceName,
        [string] $workItemType
    )
    
    $fieldResopnse = Get-Content $($global:WorkItemTypeFieldsResponsePath -f $workItemType) -Encoding UTF8 | ConvertFrom-Json       
    $filterField = $fieldResopnse | Where-Object { $_.oldFieldReferenceName -eq $referenceName }
    if($filterField.Count -gt 1){
        $fieldRefName = $filterField[0].fieldRefName
    }else
    {
        $fieldRefName = $filterField.fieldRefName
    }
    return $fieldRefName
}
# Function to handle import of WorkItemType Fields
function ImportWorkItemTypeFields {
    param (
        [PSCustomObject] $wortItemType,
        [string] $oldProcessId,
        [string] $newProcessId,
        [string] $newReferenceName
    )

    $fieldResponse = @()
    $referenceName = $wortItemType.referenceName
    $fieldsFilePath = $global:WorkItemTypeFieldsFilePath -f $oldProcessId, $referenceName    
       
    if ((Test-Path $fieldsFilePath -PathType Leaf)) {
        $fields = Get-Content $fieldsFilePath -Encoding UTF8 | ConvertFrom-Json       
        
        foreach ($field in $fields ) {  
            
            $fieldReferenceName = $field.referenceName            
            $existingField = GetExistingField $fieldReferenceName $field.name
            
            if ($null -ne $existingField) {#skip if found
                $fieldReferenceName = $existingField.referenceName                
                $fieldResponse += PrepareWorkItemTypeFieldsResponse $wortItemType $existingField $newProcessId $field.referenceName
                Write-Host " [Field] $($field.name) already present" -ForegroundColor Gray
            }
            else {                 
                $fieldBody = PrepareNewField $field      

                if($field.isPicklist)
                { 
                    $picklistId = ImportPickList $field
                    $fieldBody | Add-Member -MemberType NoteProperty -Name "picklistId" -Value $picklistId
                }
                $response = PostContent -uri $global:ProcessFieldsUri -requestBody $fieldBody -skip $true
                if ($null -ne $response) {
                    $fieldReferenceName = $response.referenceName   
                    $fieldResponse += PrepareWorkItemTypeFieldsResponse $wortItemType $response $newProcessId $field.referenceName
                }                
            }
            $workItemTypeFieldBody = @{
                "name"          = $field.name;
                "type"          = $field.type;
                "description"   = $field.description;
                "referenceName" = $fieldReferenceName;
                "defaultValue"  = $field.defaultValue;
                "customization" = $field.customization;
                "isLocked"      = $field.isLocked;
            } 

            if ($field.required -eq $true) {
                $workItemTypeFieldBody["required"] = $field.required
            }
            else {
                if ($field.type -eq "boolean") {
                    $workItemTypeFieldBody["required"] = $true
                    if ($null -eq $field.defaultValue) {
                        $workItemTypeFieldBody["defaultValue"] = "0"
                    }
                    else {
                        $workItemTypeFieldBody["defaultValue"] = $field.defaultValue
                    }
                }
            }
    
            $witfieldResponse = PostContent -uri $($global:WorkItemTypeFieldsUri -f $newProcessId, $newReferenceName) -requestBody $workItemTypeFieldBody -skip $true
            if ($witfieldResponse.setRequired -eq $true) {
                $required = $true
                if ($field.required -eq $true) {
                    $required = $false
                }
                $workItemTypeFieldBody["required"] = $required
                $witfieldResponse = PostContent -uri $($global:WorkItemTypeFieldsUri -f $newProcessId, $referenceName) -requestBody $workItemTypeFieldBody -skip $true
            }
            if ($null -ne $witfieldResponse) {
                LogMessage -type "Info" -message $(" [Field] " + $witfieldResponse.referenceName + " is registerd on $referenceName successfully.") -errorTrace $null -writeHost $true
            } 
        }

        if ($fieldResponse.Count -gt 0) {
            $fieldResponse | ConvertTo-Json | Out-File ($global:WorkItemTypeFieldsResponsePath -f $newReferenceName)
        }       
    }    
}

# ImportPickList function import new picklist
function ImportPickList {
    param (
        [PSCustomObject] $field
    )
    # if existing picklist is present then use it.  
    $picklistId = $field.picklistId
    $pickListUrl = $global:GetPickListUri
    $existingPickListResponse = GetContent $pickListUrl
    if ($null -ne $existingPickListResponse) {
        $existingPickList = $existingPickListResponse.value | Where-Object { $_.name -eq $field.pickList.name }
        if ($null -ne $existingPickList) {
            $picklistId = $existingPickList.id
            return $picklistId
        }
    }
    # if existing picklist not present then create new and use it.    
    $field.pickList.id = $null
    $pickListResponse = PostContent -uri $pickListUrl -requestBody $field.pickList -skip $true
    if ($null -ne $pickListResponse) {
        $picklistId = $pickListResponse.id
    }
    return $picklistId
}

#WorkItemType Fields Response
function PrepareWorkItemTypeFieldsResponse {
    param (
        [PSCustomObject] $wortItemType,
        [PSCustomObject] $newFieldResponse,
        [string] $newProcessId,
        [string]$oldFieldReferenceName
    )
    
    return @{
        "fieldRefName"          = $newFieldResponse.referenceName;
        "name"                  = $newFieldResponse.name;
        "processId"             = $newProcessId;
        "witRefName"            = $wortItemType.referenceName;
        "oldFieldReferenceName" = $oldFieldReferenceName
    }
}

#Function to handle preparation of new field
function PrepareNewField {
    param (
        [PSCustomObject] $field
    )
    return  @{
        "name"                = $field.name
        "description"         = $field.description
        "type"                = $field.type
        "usage"               = $field.usage
        "readOnly"            = $field.readOnly
        "canSortBy"           = $field.canSortBy
        "isQueryable"         = $field.isQueryable
        "supportedOperations" = $field.supportedOperations
        "isIdentity"          = $field.isIdentity
        "isPicklist"          = $field.isPicklist
        "isPicklistSuggested" = $field.isPicklistSuggested
    }
}


# Function to handle import of WorkItemType Pages
function ImportWorkItemTypePage {
    param (
        [PSCustomObject]$page,
        [PSCustomObject] $wortItemType,
        [string] $newProcessId,
        [string] $newReferenceName
    )

    if ($page.locked -eq $true) {
        return $pageResponse
    }

    if ($page.inherited -eq $true) {
        UpdateSystemWorkItemTypePage $page $wortItemType $newReferenceName
        return $pageResponse
    }

    $param = PreparePageRecord $page
    $uri = $global:CreateWorkItemTypePageUri -f $newProcessId, $newReferenceName
    $pageResponse = PostContent -uri $uri -requestBody $param 
    if ($null -ne $pageResponse) {
        LogMessage -type "Info" -message $(" [Page] " + $pageResponse.label + " is created on $($wortItemType.referenceName) successfully.") -errorTrace $null -writeHost $true
    }
    return $pageResponse
}

# Function to handle Prepare new page record
function PreparePageRecord {
    param (
        [PSCustomObject]$page
    )
    return @{
        "id"           = $page.id;
        "label"        = $page.label;
        "order"        = $page.order;
        "overridden"   = $page.overridden;
        "inherited"    = $page.inherited;
        "visible"      = $page.visible ;
        "locked"       = $false;
        "pageType"     = $page.pageType;
        "contribution" = $page.contribution
    }
}

# Function to handle Initialize WorkItemType Page
function UpdateSystemWorkItemTypePage {
    param (
        [PSCustomObject]$page,
        [PSCustomObject] $wortItemType,
        [string] $newReferenceName
    )
    $param = PreparePageRecord $page
    $uri = $global:CreateWorkItemTypePageUri -f $newProcessId, $newReferenceName   
    $pageresponse = PatchContent -uri $uri -requestBody $param -skip $true
    if ($null -ne $pageResponse) {
        LogMessage -type "Info" -message $(" [Page] " + $pageResponse.label + " is modified on $newReferenceName successfully.") -errorTrace $null -writeHost $true
    }
}

# Function to handle Update of System WorkItemType Page Group
function UpdateSystemWorkItemTypePageGroup {
    param (
        [PSCustomObject]$group,
        [string] $uri
    )
    $pageresponse = PatchContent -uri $uri -requestBody $group -skip $true
    if ($null -ne $pageResponse) {
        LogMessage -type "Info" -message $(" [Group] " + $groupResponse.label + " is updated on page successfully.") -errorTrace $null -writeHost $true
    }
}
# Function to handle import of groups
function ImportWorkItemTypeGroup {
    param (
        [PSCustomObject]$group,
        [string] $referenceName,
        [string] $newProcessId,
        [string] $pageId,
        [string] $sectionId
    )

    
    $param = @{
        "id"         = $group.id;
        "label"      = $group.label;
        "order"      = $group.order;
        "overridden" = $group.overridden;
        "inherited"  = $group.inherited;
        "visible"    = $group.visible ;
    }    

    # Retunn null if this group is syatem group, as we dont migrate syatem group
    if ($group.inherited -eq $true ) {
        $updateUri = $global:UpdateWorkItemTypeGroupUri -f $newProcessId, $referenceName, $pageId, $sectionId ,$group.id
        UpdateSystemWorkItemTypePageGroup $param $updateUri
        return $group
    }
        
    # Retunn null if this group is syatem group, as we dont migrate syatem group
    if ($group.controls[0].controlType -eq "HtmlFieldControl") {
        return $group
    }
    $uri = $global:CreateWorkItemTypeGroupUri -f $newProcessId, $referenceName, $pageId, $sectionId 
    $groupResponse = PostContent -uri $uri -requestBody $param  
    if ($null -ne $groupResponse) {
        LogMessage -type "Info" -message $(" [Group] " + $groupResponse.label + " is created on page successfully.") -errorTrace $null -writeHost $true
    }
 
    return $groupResponse
}

# Function to handle update system of control
function UpdateWorkItemTypeControl {
    param (
        [PSCustomObject]$control,
        [string] $referenceName,
        [string] $newProcessId,
        [string] $pageId,
        [string] $groupId,
        [string] $sectionId
    )

    $param = @{
        "id"          = $control.id;    
        "label"       = $control.label;
        "readOnly"    = $control.readOnly;
        "visible"     = $control.visible;
        "controlType" = $control.controlType;
        "metadata"    = $control.metadata;
    }

    $uri = $global:UpdateWorkItemTypeControlUri -f $newProcessId, $referenceName, $groupId, $control.id
    $controlResponse = PatchContent -uri $uri -requestBody $param
    if ($null -ne $controlResponse) {
        LogMessage -type "Info" -message $(" [Control] " + $controlResponse.label + " is updated on group successfully.") -errorTrace $null -writeHost $true
    }

}

# Function to handle import of control
function ImportWorkItemTypeControl {
    param (
        [PSCustomObject]$control,
        [string] $referenceName,
        [string] $newProcessId,
        [string] $pageId,
        [string] $groupId,
        [string] $sectionId
    )


    $newFieldReferenceName = GetNewFieldReferenceName $control.id $referenceName
    if ($null -ne $newFieldReferenceName) {
        $control.id = $newFieldReferenceName
    }
    else {
        $existingField = GetExistingField $control.id $control.label
        $control.id = $existingField.referenceName
    }
    
    $param = @{
        "id"             = $control.id;    
        "order"          = $control.order;
        "label"          = $control.label;
        "readOnly"       = $control.readOnly;
        "visible"        = $control.visible;
        "controlType"    = $control.controlType;
        "isContribution" = $control.isContribution;
        "height"         = $control.height;
        "metadata"       = $control.metadata;
        "inherited"      = $control.inherited;
        "overridden"     = $control.overridden;
        "watermark"      = $control.watermark;
    }

    if ($control.controlType -eq "HtmlFieldControl") {
        ImportHtmlFieldControl $control $referenceName $newProcessId $pageId $groupId $sectionId
    }
    else {
        $uri = $global:CreateWorkItemTypeControlUri -f $newProcessId, $referenceName, $groupId
        $controlResponse = PostContent -uri $uri -requestBody $param
        if ($null -ne $controlResponse) {
            LogMessage -type "Info" -message $(" [Control] " + $controlResponse.label + " is added on group successfully.") -errorTrace $null -writeHost $true
        }
    }
}

# Function to handle Import of HtmlFieldControl
function ImportHtmlFieldControl {
    param (
        [PSCustomObject]$htmlControl,
        [string] $referenceName,
        [string] $processId,
        [string] $pageId,
        [string] $groupId,
        [string] $sectionId
    )
    
    if ($null -eq $global:grouporder ) {
        $global:grouporder = 5
    }
    else {
        $global:grouporder += 1
    }

    
    $newGuid = [Guid]::NewGuid()
    $group = @{
        "id"         = $newGuid;
        "label"      = $htmlControl.label;
        "order"      = "$global:grouporder";
        "overridden" = $false;
        "inherited"  = $true;
        "visible"    = $true ;
        "controls"   = @($htmlControl) 
    }
    $uri = $global:CreateWorkItemTypeGroupUri -f $newProcessId, $referenceName, $pageId, $sectionId 
    $groupResponse = PostContent -uri $uri -requestBody $group  
    if ($null -ne $groupResponse) {
        LogMessage -type "Info" -message $(" [Control] " + $htmlControl.label + " is added on group successfully.") -errorTrace $null -writeHost $true
    }
}

# Function to get newlly created page id or existing page id
function GetIdFromResponse {
    param (
        [PSCustomObject]$response,
        [PSCustomObject]$orgObject
    )
    
    $pageId = $orgObject.id

    if (-not ( $null -eq $response.id) ) {
        $pageId = $response.id

    } 

    return $pageId
}

# Function to handle the preparation of new project request json
function PrepareNewProject {
    param (
        [PSCustomObject]$projectObject
    )

    $projectDetails = Get-Content $($global:ProjectDetailsFilePath -f $projectObject.id) -Encoding UTF8 | ConvertFrom-Json

    $processes = Get-Content -Path $global:ProcessesFilePath -Raw -Encoding UTF8 | ConvertFrom-Json
    $filteredRecord = $processes  | Where-Object { $_.typeId -eq $projectDetails.capabilities.processTemplate.templateTypeId }

    $processesResponse = Get-Content -Path  $global:ProcessesResponsePath -Raw -Encoding UTF8 | ConvertFrom-Json
    $filteredResponseRecord = $processesResponse | Where-Object { $_.oldTypeId -eq $filteredRecord.typeId }

    # Create a PowerShell custom object with properties
    $new_project = [PSCustomObject]@{
        "name"         = $projectObject.name
        "description"  = $projectObject.description
        "capabilities" = @{
            "versioncontrol"  = @{
                "sourceControlType" = $projectDetails.capabilities.versioncontrol.sourceControlType
            }
            "processTemplate" = @{
                "templateTypeId" = $filteredResponseRecord.newTypeId
            }
        }
    } 
    return $new_project 
}

# Function to handle the preparation of new project response
function PrepareNewProjectResponse {
    param (
        [PSCustomObject]$project,
        [PSCustomObject]$new_project_response,
        [string] $newProjectId,
        [PSCustomObject]$new_project_param
    )

    $projectResponse = @{
        "oldProjectId" = $project.id;
        "newProjectId" = $newProjectId;
        "name"         = $project.name;
        "url"          = $project.url;
        "state"        = $project.state;
        "revision"     = $project.revision;
        "visibility"   = $project.visibility;
        "status"       = $new_project_response.status;
        "operationId"  = $new_project_response.id;
        "processId"    = $new_project_param.capabilities.processTemplate.templateTypeId;
    }
    return $projectResponse
}

# Function to get project details from exported project
function GetProjectDetails {
    param (
        [PSCustomObject]$exportedProject
    )

    $projectDetails = Get-Content $($global:ProjectDetailsFilePath -f $exportedProject.id) -Encoding UTF8 | ConvertFrom-Json

    $newProjectDetails = [PSCustomObject]@{
        "name"           = $exportedProject.name
        "description"    = $exportedProject.description        
        "lastUpdateTime" = $projectDetails.lastUpdateTime
        "revision"       = $projectDetails.revision
        "visibility"     = $projectDetails.visibility
    }
    
    return $newProjectDetails 
}
 
# Function to get newlly created project Id
function GetNewProjectId {
    param (
        [string]$projectName
    )
    # get newlly created projects 
    $response = GetContent $($global:GetProjectUri -f $projectName)
    return $response.id
}

# Function to handle the import of projects
function ImportProjects {

    $projectResponseList = @()
    $projects = Get-Content $global:ProjectFilePath -Encoding UTF8 | ConvertFrom-Json
    if ($null -eq $projects) {
        LogMessage -type "Info" -message $(" [Project] There are not exported projects to import.") -errorTrace $null -writeHost $true
    }
    else {
    
        foreach ($project in $projects) {
            $params = PrepareNewProject $project
            $response = PostContent $global:CreateProjectUri $params
            LogMessage -type "Info" -message $(" [Project] " + $project.name + " project is imported successfully") -errorTrace $null -writeHost $true
            Show-Loader -totalSteps 7 -delayMilliseconds 300
            $newProjectId = GetNewProjectId $params.name
        
            # Add request details to response and save as a JSON
            $projectResponseList += PrepareNewProjectResponse $project $response $newProjectId $params
        }
    
        if ($projectResponseList.Count -gt 0) {
            $projectResponseList | ConvertTo-Json |  Out-File $global:ProjectResponsePath
        }
    }
}

# Function to handle the preparation of new proccess workItemType
function PrepareNewProccessWorkItemType {
    param (
        [PSCustomObject]$proccessWorkItemType
    )
    
    $work_item_type = @{
        "name"         = $proccessWorkItemType.name;
        "description"  = $proccessWorkItemType.description;
        "color"        = $proccessWorkItemType.color;
        "icon"         = $proccessWorkItemType.icon;
        "isDisabled"   = $proccessWorkItemType.isDisabled;
        "inheritsFrom" = $proccessWorkItemType.inheritsFrom
    }
    
    return $work_item_type
}

# Function to handle the Import of Proccess WorkItemType Import
function ImportProccessWorkItemType {   
    $processes = Get-Content  $global:ProcessesResponsePath -Encoding UTF8 | ConvertFrom-Json
    foreach ($process in $processes) {
        $oldTypeId = $process.oldTypeId
        $newProcessId = $process.newTypeId
        $processe_wortItemTypes = Get-Content $($global:WorkItemTypeFilePath -f $oldTypeId , $process.name) -Encoding UTF8 | ConvertFrom-Json
        foreach ($wortItemType in $processe_wortItemTypes.value | Where-Object { $_.customization -ne "system" }) {
            $newReferenceName = $wortItemType.referenceName            

            if ($wortItemType.customization -eq "custom" ) {
                $param = PrepareNewProccessWorkItemType $wortItemType
                $uri = $global:CreateWorkItemTypeUri -f $newProcessId 
                $response = PostContent $uri $param
                $newReferenceName = $response.referenceName
            }
            else
            {
                InitializeWorkItemType $wortItemType $newProcessId
                $newReferenceName = GetExistingReferenceName $newProcessId $wortItemType.inherits
            }           
            ImportWorkItemTypeLayout $wortItemType $oldTypeId $newProcessId $newReferenceName
            LogMessage -type "Info" -message $(" [WorkItemType] Work Item Type " + $wortItemType.name + " is imported successfully.") -errorTrace $null -writeHost $true
        }
    }  
}

# Function to Get Existing WorkItemType Reference Name
function GetExistingReferenceName {
    param (
        [string]$newProcessId ,
        [string]$inherits 
    )
    $response = GetContent -uri $($global:CreateWorkItemTypeUri -f $newProcessId)
    $filteredResponse = $response.value | Where-Object {$_.inherits -eq $inherits }
    return $filteredResponse.referenceName
}

# Function will Initialize WorkItemType
function InitializeWorkItemType {
    param (
        [PSCustomObject]$wortItemType,
        [string]$processId
    )

    try {
        $param = @{
            "name"         = $wortItemType.name;
            "inheritsFrom" = $wortItemType.inherits;
            "color"        = $wortItemType.color;
            "description"  = $wortItemType.description;
            "icon"         = $wortItemType.icon;
            "isDisabled"   = $false
        }
        $uri = $($global:CreateWorkItemTypeUri -f $processId)
        $response = PostContent $uri $param
        LogMessage -type "Info" -message $(" [wortItemType] " + $response.inherits + " is configured successfully.") -errorTrace $null -writeHost $true
    }
    catch {
        LogMessage -type "Info" -message $(" [wortItemType] " + $response.inherits + " is already present.") -errorTrace $null -writeHost $true
    }    
}

# Function to handle the Import of organization processes
function ImportAzureDevopsProcess {  
  
    $processResponseList = @()
    $processes = Get-Content $global:ProcessesFilePath -Encoding UTF8 | ConvertFrom-Json
    foreach ($process in $processes) {
        if ($process.customizationType -ne "system") {
            $params = @{
                "name"                = $process.Name;
                "parentProcessTypeId" = $process.parentProcessTypeId;
                "referenceName"       = GetNewReferenceName $process.referenceName;
                "description"         = $process.description;
            }

            $response = PostContent $global:CreateProcessesUri $params
            LogMessage -type "Info" -message " [Process] $($process.name) process is imported successfully." -errorTrace $null -writeHost $true

            # Add request details to response and save as a JSON
            $params.oldTypeId = $process.typeId
            $params.name = $process.name
            $params.parentProcessTypeId = $process.parentProcessTypeId
            $params.newTypeId = $response.typeId
            # Convert the response to JSON
            $processResponseList += $params 
        }                                 
    }

    $jsonData = $processResponseList | ConvertTo-Json
    $jsonData |  Out-File $global:ProcessesResponsePath            
}

# This function prepare new reference name by adding NewGuid 
function GetNewReferenceName {
    param (
        [string]$referenceName
    )

    # Generate a new GUID
    $newGuid = [Guid]::NewGuid()

    # Split the original string by the dot (.)
    $parts = $referenceName -split '\.', 2

    # Check if there is at least one dot in the string
    if ($parts.Length -gt 1) {
        # Replace the part after the first dot with the new GUID
        $referenceName = $parts[0] + '.' + $($newGuid.ToString() -replace '-', '')
    }   

    return $referenceName
}
# UpdateAzureDevopsProcessName update the imported process name with Alias value after import complate
function UpdateAzureDevopsProcessName {    
    $processes = Get-Content $global:ProcessesResponsePath -Encoding UTF8 | ConvertFrom-Json
    $configProcesses = $global:ConfigProcesses

    if ($null -ne $configProcesses) {
        $processes | ForEach-Object {
            $processName = $_.name
            $processTypeId = $_.newTypeId
            $configProcesse = $configProcesses | Where-Object { $_.name -eq $processName }
            if ($null -ne $configProcesse.Alias -and $configProcesse.Alias -ne "") {
                $param = @{
                    "name"        = $configProcesse.Alias;
                    "description" = $_.description;
                    "IsDefault"   = $_.IsDefault;
                    "IsEnabled"   = $_.IsEnabled
                }
                $response = PatchContent -uri $($global:RenameAzureDevopsProcessUri -f $processTypeId) -requestBody $param
                if ($null -ne $response) {
                    LogMessage -type "Info" -message $(" Process name " + $processName + " has been updated to  $($configProcesse.Alias)  successfully.") -errorTrace $null -writeHost $true
                }
            }
        }
    }
}

# UpdateAzureDevopsProcessName update the imported process name with Alias value after import complate
function UpdateAzureDevopsProjectsName {    
    $projects = Get-Content $global:ProjectResponsePath -Encoding UTF8 | ConvertFrom-Json
    $configProcesses = $global:ConfigProcesses
    if ($null -ne $configProcesses) {
        foreach ($configProcess in $configProcesses) {
            foreach ($project in $configProcess.Projects) {
                $projectName = $project.ProjectName
                $aliasProjectName = $project.AliasProjectName
                $configProject = $projects | Where-Object { $_.name -eq $projectName }

                # Your code to update the project goes here
                # Use $projectName, $aliasProjectName, and $configProject variables as needed

                # Example:
                if ($null -ne $configProject) {
                    $param = @{
                        "name"        = $aliasProjectName 
                    }
                    $response = PatchContent -Uri $($global:RenameDestAzureDevopsProjectUri -f $configProject.newProjectId) -RequestBody $param
                    if ($null -ne $response) {
                        LogMessage -Type "Info" -Message $("Process name " + $projectName + " has been updated to $($aliasProjectName) successfully.") -ErrorTrace $null -WriteHost $true
                    }
                }
            }
        }
    } 
}

# GetPojectsForProcess returns projects for each process from exported data as per there templateTypeId relation
function GetPojectsForProcess {
    param (
        [string] $proccessId
    )

    $param = @()
    if (Test-Path $global:ProjectFilePath -PathType Leaf) { 
        $projects = Get-Content $global:ProjectFilePath -Encoding UTF8 | ConvertFrom-Json
        if ($null -ne $projects) {
            foreach ($project in $projects) {
                $projectId = $project.id
                $projectDetails = Get-Content $($global:ProjectDetailsFilePath -f $projectId) -Encoding UTF8 | ConvertFrom-Json
                $detail = $projectDetails  | Where-Object { $_.capabilities.processTemplate.templateTypeId -eq $proccessId }
                if (-not( $null -eq $detail)) {
                    $param += @{
                        "ProjectName" = $detail.name;
                        "ProjectId"   = $detail.id
                    }
                }
            }
        }
        else {
            ShowErrorMessage -type "Error" -message "There is no exported project. Please start Import with atleast one Project" 
            exit 1
        }
    }
    else {
        ShowErrorMessage -type "Error" -message "Unable to find exported project file. Please start Import with Projects" 
        exit 1
    }

    return  $param | ConvertTo-Json
}

# formatImportDetails returns projects for each process from Import config
function formatImportDetails {
    param (
        [PSCustomObject] $process
    )
    $processName = $process.Name
    
    if ($process.Projects -eq "*") {
        $projectNames = "*"
    }
    elseif ($process.Projects.Count -gt 1) {
        $projectNames = $process.Projects.ProjectName -join ', '
    }    
    else {
        $projectNames = $process.Projects[0].ProjectName
    }
    
    return  @{
        ProcessName = $processName
        Projects    = $projectNames
    }
}

# GetDistinctArray returns the Unique Projects from the collection
function GetDistinctArray {
    param (
        [array] $array
    )
    
    $distinct_array = $array | ForEach-Object {
        [PSCustomObject]@{
            ProcessName = $_.ProcessName
            Projects    = $_.Projects -Split ","
        }
    } | Select-Object -Property ProcessName, Projects -Unique
    
    return $distinct_array
}

# ImportAzureDevOps list all the import function and execute them
function ImportAzureDevOps {
    
    Write-Host " Process import started *******************" -ForegroundColor Yellow    
    ImportAzureDevopsProcess
    Write-Host " Process import finished *******************" -ForegroundColor Green    

    Write-Host " Projects import started *******************" -ForegroundColor Yellow    
    ImportProjects
    Write-Host " Projects import finished *******************" -ForegroundColor Green    

    Write-Host " Work Item Type import started *******************" -ForegroundColor Yellow  
    ImportProccessWorkItemType
    Write-Host " Work Item Type import finished *******************" -ForegroundColor Green
    
    Write-Host " Queries import started *******************" -ForegroundColor Yellow    
    ImportQueries
    Write-Host " Queries import finished *******************" -ForegroundColor Green

    Write-Host " Dashboard import started *******************" -ForegroundColor Yellow
    ImportDashboardWithWidgets
    Write-Host " Dashboard import finished *******************" -ForegroundColor Green   

    UpdateAzureDevopsProcessName

    UpdateAzureDevopsProjectsName

    ArchiveFile
}

# ConfirmImport function comapre the import configuration and exported data and 
# ask for confimation to proceed with import, it show the configuration details
function ConfirmImport {           
    $exported_processes = Get-Content $global:ProcessesFilePath  -Encoding UTF8 | ConvertFrom-Json

    $exported_orgConfig = Get-Content $global:ExportOrgConfigFilePath  -Encoding UTF8 | ConvertFrom-Json

    $Processes = @()
    foreach ($exp_process in $exported_processes) {
        $exported_projects = GetPojectsForProcess $exp_process.typeId    
        $projectData = $exported_projects | ConvertFrom-Json | ForEach-Object {
            @{
                "ProjectName" = $_.ProjectName
                "ProjectId"   = $_.ProjectId
            }
        }    
        $Process = @{
            "name"     = $exp_process.name
            "typeId"   = $exp_process.typeId
            "Projects" = $projectData
        }    
        $Processes += $Process
    }

    $configProcessesCollection = $global:ConfigProcesses
    $ProcessesJson = @($Processes) | ConvertTo-Json
    $global:ExportedProcesses = $ProcessesJson | ConvertFrom-Json
    $exportedProcessesCollection = @($global:ExportedProcesses)
    $notEqualRecords = @()
    $exportedlRecords = @()

    if ($configProcessesCollection.Count -ne $exportedProcessesCollection.Count) {
        foreach ($configProcess in $configProcessesCollection) {
            $notEqualRecords += @{
                "config_record" = $configProcess
            }
        }
        $exportedlRecords += @($exportedProcessesCollection)
    }
    else {      
        foreach ($configProcess in $configProcessesCollection) {
            if ($configProcess -ne "*") {
        
                $matchingProcesses = $exportedProcessesCollection | Where-Object { $_.Name -eq $configProcess.name }

                if ($null -ne $matchingProcesses) {
                    $configProjects = $configProcess.Projects | Sort-Object -Property ProjectName
                    $matchingProjects = $matchingProcesses.Projects | Sort-Object -Property ProjectName
                    if ($configProjects -ne "*") {
                        foreach ($configProject in $configProjects) {
                            $matchingProject = $matchingProjects | Where-Object { $_.ProjectName -eq $configProject.ProjectName }
                            if ($null -eq $matchingProject) {
                                $notEqualRecords += @{
                                    "config_record" = $configProcess
                                }
                                $exportedlRecords += @($matchingProcesses)
                                break
                            }
                        }                    
                    }
                }
                else {
                    $notEqualRecords += @{
                        "config_record" = $configProcess
                    }
                    $exportedlRecords += @($exportedProcessesCollection)

                }
            }
        }
    }
    if ($notEqualRecords.Count -gt 0) {
        ShowErrorMessage -type "Error" -message "Exported and Import Config records are not equal :"      
        $configRecordOutput = New-Object System.Collections.ArrayList
        $exportRecordOutput = New-Object System.Collections.ArrayList
        
        # Convert objects to JSON strings and filter out duplicates
        $uniqueObjects = $exportedlRecords | ForEach-Object { $_ | ConvertTo-Json -Depth 100 } | Select-Object -Unique

        # Convert JSON strings back to PowerShell objects
        $exportedlRecords = $uniqueObjects | ConvertFrom-Json

        foreach ($record in $notEqualRecords) {
            if ( $null -ne $record.config_record) {
                $configRecordOutput += $(formatImportDetails $record.config_record )
            }           
        }
        if ( $null -ne $exportedlRecords) {
            $exportRecordOutput += $(formatImportDetails $exportedlRecords )
        }
        $messageStr = "----------------------------------------------------------------------------"
        ShowErrorMessage -type "Error" -message $messageStr 
        $messageStr = "Import Config Record : "
        ShowErrorMessage -type "Error" -message $messageStr  
        $configRecordOutput | ForEach-Object {
            $processName = "- Process : $($_.ProcessName)"
            ShowErrorMessage -type "Error" -message $processName
            $projects = "  + Projects : $($_.Projects )"
            ShowErrorMessage -type "Error" -message $projects
        }
        $messageStr = "Exported Record :"
        ShowErrorMessage -type "Error" -message $messageStr 
        $exportRecordOutput | ForEach-Object {
            $processName = "- Process : $($_.ProcessName)"
            ShowErrorMessage -type "Error" -message $processName
            $projects = "  + Projects : $($_.Projects)"
            ShowErrorMessage -type "Error" -message $projects
        }
        $messageStr = "Import process is terminated. Please correct the import configuration as per export data." 
        ShowErrorMessage -type "Error" -message $messageStr
        $messageStr = "----------------------------------------------------------------------------"
        ShowErrorMessage -type "Error" -message $messageStr
    }
    else {
        Write-Host "----------------------------------------------------------------------------"
        foreach ($item in $exportedProcessesCollection) {
            $processName = $item.name
            $ExportOrgUrl = $exported_orgConfig.ExportOrgUrl
            Write-Host("Export From : $ExportOrgUrl") 
            Write-Host ("+ $processName ") 
            foreach ($project in $item.Projects) {
                $projectName = $project.ProjectName
                Write-Host "  -  $projectName" 
            }
        }
        $exportChoice = Read-Host "Do you want to import the processes and projects (Y/N)?"
        Write-Host "----------------------------------------------------------------------------"
        if ($exportChoice -eq 'Y' -or $exportChoice -eq 'y') {
            LogMessage -type "Info" -message  " Import started ........" -errorTrace $null -writeHost $true
            ImportAzureDevOps
        }
        else {
            LogMessage -type "Info" -message  " Import has been aborted ........" -errorTrace $null -writeHost $true
        }
    }
}

# ImportAzureDevOps function call Import Config Validation and extract the zip data
function ValidateAndInitiateImport {   
    param (
    [PSCustomObject]$importconfigFile
    )
    if (Test-Path -Path $PSScriptRoot -PathType Container) {
        LogMessage -type "Info" -message  " Import Config Validation Started ..." -errorTrace $null -writeHost $true
        
        $jsonContent = Get-Content -Path $importconfigFile -Encoding UTF8
        $jsonKeyFile = Join-Path -Path $PSScriptRoot -ChildPath "RequiredKeys.json"
        $jsonRequiredKeyconfig = Get-Content -Path $jsonKeyFile | ConvertFrom-Json
        $importJsonString = $jsonRequiredKeyconfig.Keys.Import  | ConvertTo-Json 
        $isValidJson = ValidateJson -jsonString $jsonContent $importJsonString

        if ($isValidJson) {         
            LogMessage -type "Info" -message  " Import Config Validation Finished ..." -errorTrace $null -writeHost $true     
            # Delete existing Import Path
            DeleteImportPath

            # Check if the zip file folder exists, if not, create it
            if (-Not (Test-Path -Path $global:ImportZipFilePath -PathType Container)) {
                ShowErrorMessage -type "Error" -message  " Folder is not present: $global:ImportZipFilePath" 
                ShowErrorMessage -type "Error" -message "Import Config data is not valid. Please provide valid Import Config data."
                exit 1
            }

            $zipFilePath = $global:ImportZipFilePath + "\" + $global:ImportZipFileName
            # Check if the zip file folder exists, if not, create it
            if (-Not (Test-Path $zipFilePath -PathType Leaf)) {
                ShowErrorMessage -type "Error" -message  " Import zip file '$global:ImportZipFileName' is not present at the configured location " 
                ShowErrorMessage -type "Error" -message "Import Config data is not valid. Please provide valid Import Config data."
                exit 1
            }

            # Check if the destination folder exists, if not, create it
            if (-Not (Test-Path -Path $global:ImportFolderPath -PathType Container)) {
                New-Item -ItemType Directory -Force -Path $global:ImportFolderPath
                LogMessage -type "Info" -message  " Folder created: $global:ImportFolderPath" -errorTrace $null -writeHost $true
            }
            else {
                LogMessage -type "Info" -message  " Folder already exists: $global:ImportFolderPath" -errorTrace $null -writeHost $true
            }    

            # Unzip the contents of the ZIP file to the destination folder
            Expand-Archive -Path $zipFilePath -DestinationPath $global:ImportFolderPath -Force
            
            # List of folders to be created
            $foldersToCreate = @( 
                "Processes\ProcessesTypeResponse",
                "Projects\ProjectResponse",
                "Queries\QueriesResponse",
                "ProcessWorkItemTypes\WorkItemTypeFields\FieldsResponse"
            ) 
            # Loop through each folder and create if it doesn't exist
            foreach ($folder in $foldersToCreate) {
                $folderPath = Join-Path -Path $global:ImportFolderPath -ChildPath $folder
                if (-Not (Test-Path $folderPath -PathType Container)) {
                    New-Item -ItemType Directory -Path $folderPath | Out-Null
                    LogMessage -type "Info" -message  " Folder created $folderPath" -errorTrace $null -writeHost $true
                }
                else {
                    LogMessage -type "Info" -message  " Folder already exists: $folderPath" -errorTrace $null -writeHost $true
                }
            }  
              
            ConfirmImport
        }
        else {
            # Stop further processing or display an error message
            ShowErrorMessage -type "Error" -message "Import Config data is not valid. Please provide valid Import Config data."
            exit 1
        }         
    }
    else {
        Write-Output "$PSScriptRoot is not a directory or doesn't exist."
        exit 1
    }    
}

# Function Start the Import
function  StartImport {  
    # Main Part
    try {
        # Assign variables from the JSON object and rest api urls as global variables  
        InitializeGlobalFields

        LogMessage -type "Info" -message " Welcome to the Azure DevOps Import Data Management Tool!" -errorTrace $null -writeHost $true
        LogMessage -type "Info" -message " Import script started." -errorTrace $null -writeHost $true

        $importconfigFile = Join-Path -Path $PSScriptRoot -ChildPath "ImportConfig.json"

        if (Test-Path $importconfigFile -PathType Leaf) {        
            
            ValidateAndInitiateImport $importconfigFile      
            
            LogMessage -type "Info" -message " Import script finished." -errorTrace $null -writeHost $true
        }
        else {
            ShowErrorMessage -type "Error" -message "No config file is present at the Import script location."
        } 
    }
    catch {     
        Rollback
        LogException $_
    }
}

StartImport