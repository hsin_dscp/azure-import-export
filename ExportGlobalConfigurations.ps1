function InitializeGlobalFields() {
    #Export configs

    $jsonFile = Join-Path -Path $PSScriptRoot -ChildPath "ExportConfig.json"
    $config = Get-Content $jsonFile -Encoding UTF8 | ConvertFrom-Json
    # Assign variables from the JSON object as global variables  
    $global:BaseAPIUrl = $config.ExportBaseAPIUrl
    $global:OrganizationName = $config.ExportOrganizationName
    $parsonalAccessToken = $config.ExportOrganizationAPIKey    
    $global:ApiKey = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(":$parsonalAccessToken"))
    $global:ExportFolderPath = $config.ExportFolderPath
    $global:ExportFileName = $config.ExportFileName
    $global:ExportErrorPath = $config.ExportErrorPath
    $global:ExportConfigPath = $config.ExportConfigFilePath
    $global:Processes = $config.Processes

    #ShowTree
    $global:ProjectFilePath = "$global:ExportFolderPath/$ProjectFolderName/Projects.json"
    $global:ProcessesFilePath = "$global:ExportFolderPath/Processes/Processes_Type.json"

    #FilterSelectedProcesses
    $global:GetProcessesUri = "$global:BaseAPIUrl/$global:OrganizationName/_apis/work/processes?api-version=7.0"

    #RetrieveFilterAllProjects`
    $global:ProjectDetailsFilePath = "$global:ExportFolderPath/ProjectDetails/Project_Details_{0}.json"
    $global:GetProjectUri = "$global:BaseAPIUrl/$global:OrganizationName/_apis/projects?api-version=7.0"
    $global:GetProjectDetailsUri = "$global:BaseAPIUrl/$global:OrganizationName/_apis/projects/{0}?api-version=7.0-preview.1"

    #RetriveAzureDevOpsQueriesForEachProject
    $global:GetProjectQueriesUri = "$global:BaseAPIUrl/$global:OrganizationName/{0}/_apis/wit/queries?`$depth=1&`$expand=all&api-version=7.2-preview.2"
    
    #RetriveAzureDevOpsChildQueries
    $global:GetProjectChildQueriesUri = "$global:BaseAPIUrl/$global:OrganizationName/{0}/_apis/wit/queries/{1}?`$depth=1&`$expand=all&api-version=7.2-preview.2"
    
    $global:ProjectQuerieFilePath = "$global:ExportFolderPath/Queries/Queries_{0}.json"

    #RetriveAzureDevOpsDashboardsForEachProject
    $global:GetDashboardUri = "$global:BaseAPIUrl/$global:OrganizationName/{0}/_apis/dashboard/dashboards?api-version=7.1-preview.3"
    $global:DashboardFilePath = "$global:ExportFolderPath/Dashboards/Dashboards_{0}.json"
   
    #RetriveDashboardWidgets
    $global:DashboardWidgetsFilePath = "$global:ExportFolderPath/Dashboards/Widgets/Widgets_{0}.json"

    #RetrieveAllWorkItemTypesForProccess
    $global:WorkItemTypesFilePath = "$global:ExportFolderPath/ProcessWorkItemTypes/Work_Item_Types_{0}_{1}.json"
    $global:GetWorkItemTypesUri = "$global:BaseAPIUrl/$global:OrganizationName/_apis/work/processes/{0}/workitemtypes?api-version=7.0"

    #RetrieveLayoutStatesRulesForAllWorkItemTypes
    $global:GetLayoutUri = "$global:BaseAPIUrl/$global:OrganizationName/_apis/work/processes/{0}/workItemTypes/{1}/layout?api-version=7.0"
    $global:GetLayoutFilePath = "$global:ExportFolderPath/ProcessWorkItemTypes/WorkItemTypeLayout/WorkItemTypeLayout_{0}_{1}.json"
    $global:GetStatesFilePath = "$global:ExportFolderPath/ProcessWorkItemTypes/WorkItemTypeStates/WorkItemTypeStates_{0}_{1}.json"
    $global:GetRulesFilePath = "$global:ExportFolderPath/ProcessWorkItemTypes/WorkItemTypeRules/WorkItemTypeRules_{0}_{1}.json"
    $global:GetFieldsFilePath = "$global:ExportFolderPath/ProcessWorkItemTypes/WorkItemTypeFields/WorkItemTypeFields_{0}_{1}.json"

    #ExportPickList
    $global:GetFieldsUri = "$global:BaseAPIUrl/$global:OrganizationName/_apis/wit/fields/{0}?api-version=7.2-preview.3 "
    $global:GetPickListUri   = "$global:BaseAPIUrl/$global:OrganizationName/_apis/work/processes/lists/{0}?api-version=7.2-preview.1"

    # ShowConfigurationDetails function logs export configuration details
    function ShowConfigurationDetails {
        Write-Host "ApiKey : $global:ApiKey"
        Write-Host "ExportFileName : $global:ExportFileName" 
        Write-Host "ExportErrorPath : $global:ExportErrorPath"
        Write-Host "Processes : $global:Processes"     
        Write-Host "ProjectFilePath : $global:ProjectFilePath"
        Write-Host "ProcessesFilePath : $global:ProcessesFilePath"
        Write-Host "GetProcessesUri : $global:GetProcessesUri"
        Write-Host "ProjectDetailsFilePath : $global:ProjectDetailsFilePath"
        Write-Host "GetProjectUri : $global:GetProjectUri"         
        Write-Host "GetProjectDetailsUri : $global:GetProjectDetailsUri"  
        Write-Host "GetProjectQueriesUri : $global:GetProjectQueriesUri" 
        Write-Host "GetProjectChildQueriesUri : $global:GetProjectChildQueriesUri"  
        Write-Host "ProjectQuerieFilePath : $global:ProjectQuerieFilePath"
        Write-Host "GetDashboardUri : $global:GetDashboardUri" 
        Write-Host "DashboardFilePath : $global:DashboardFilePath"
        Write-Host "DashboardWidgetsFilePath : $global:DashboardWidgetsFilePath"
        Write-Host "WorkItemTypesFilePath : $global:WorkItemTypesFilePath" 
        Write-Host "GetWorkItemTypesUri : $global:GetWorkItemTypesUri"   
        Write-Host "GetLayoutFilePath : $global:GetLayoutFilePath" 
        Write-Host "GetLayoutUri : $global:GetLayoutUri"      
        Write-Host "GetStatesFilePath : $global:GetStatesFilePath" 
        Write-Host "GetRulesFilePath : $global:GetRulesFilePath"  
        Write-Host "GetFieldsFilePath : $global:GetFieldsFilePath" 
        Write-Host "GetFieldsUri : $global:GetFieldsUri"
        Write-Host "GetPickListUri : $global:GetPickListUri"
        Write-Host "ExportConfigPath : $global:ExportConfigPath"
    }
}