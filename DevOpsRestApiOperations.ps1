# Load the CommanFunctions.ps1 script
. "$PSScriptRoot\CommanFunctions.ps1"

# GetContent function retrive data from devops organization 
function GetContent {
    param (
        [string]$uri
    )

    $response = $null
    
    try {

        $response = Invoke-RestMethod -Uri $uri -Headers @{Authorization = "Basic $global:ApiKey" }  -Method Get -ContentType "application/json; charset=UTF-8"
    }
    catch {
        LogRequestDetailsOnException -uri $uri -requestBody $null
        throw
    }
    return $response         
}

# PostContent function import data to the devops organization 
function PostContent {
    param (
        [string]$uri,
        [PSCustomObject]$requestBody,
        [bool]$skip = $false
    )

    $response = $null
    
    try {

        $response = Invoke-RestMethod -Uri $uri -Headers @{Authorization = "Basic $global:ApiKey" }  -Method Post -Body ($requestBody | ConvertTo-Json -Depth 25) -ContentType "application/json; charset=UTF-8"
    }
    catch {
        Write-Host $uri
        Write-Host $requestBody | ConvertTo-Json
        if($skip -eq $true)
        {
            if ($null -ne $_.ErrorDetails) {
                $message = $_.ErrorDetails.Message | ConvertFrom-Json
                if ($message.message -like "*The 'required' property is not editable on field*") {
                    $response = @{
                        "setRequired" = $true
                    }
                }               
            }
        }
        if ($skip -eq $false) {
            LogRequestDetailsOnException -uri $uri -requestBody $requestBody
            throw
        }
    }
    return $response         
}

# PutContent function update data to the devops organization 
function PutContent {
    param (
        [string]$uri,
        [PSCustomObject]$requestBody
    )

    $response = $null

    try {
        $response = Invoke-RestMethod -Uri $uri -Headers @{Authorization = "Basic $global:ApiKey" }  -Method Put -Body ($requestBody | ConvertTo-Json -Depth 10)  -ContentType "application/json; charset=UTF-8"
    }
    catch {
        LogRequestDetailsOnException -uri $uri -requestBody $requestBody
        throw
    }

    return  $response 
}

# PatchContent function update data to the devops organization 
function PatchContent {
    param (
        [string]$uri,
        [PSCustomObject]$requestBody
    )

    $response = $null

    try {
        $response = Invoke-RestMethod -Uri $uri -Headers @{Authorization = "Basic $global:ApiKey" }  -Method Patch -Body ($requestBody | ConvertTo-Json -Depth 10)  -ContentType "application/json; charset=UTF-8"
    }
    catch {
        LogRequestDetailsOnException -uri $uri -requestBody $requestBody
        throw
    }

    return  $response 
}

# DeleteContent function delete data from the devops organization 
function DeleteContent {
    param (
        [string]$uri
    )
    
    $response = $null

    try {

        $response = Invoke-RestMethod -Uri $uri -Headers @{Authorization = "Basic $global:ApiKey" }  -Method Delete  -ContentType "application/json"
    }
    catch {
        LogRequestDetailsOnException -uri $uri -requestBody $null
        LogException $_
        exit 1
    }

    return  $response 
}