# Load the DevOpsRestApiOperations.ps1 script
. "$PSScriptRoot\DevOpsRestApiOperations.ps1"
#. "$PSScriptRoot\ImportGlobalConfigurations.ps1"
# Load the LoggingFunctions.ps1 script
. "$PSScriptRoot\CommanFunctions.ps1"
function Rollback {
    try {
        #InitializeGlobalFields
        DeleteFields
        Show-Loader -totalSteps 7 -delayMilliseconds 300
        DeleteProjects
        Show-Loader -totalSteps 7 -delayMilliseconds 300
        DeleteProcesses
    }
    catch {
        LogException $_
    }    
}

# DeleteFields funtion delete's imported fields
function DeleteFields {
    if ((Test-Path $global:WorkItemTypeFieldsResponsePath -PathType Leaf)) {
        $workItemTypeFields = Get-Content $global:WorkItemTypeFieldsResponsePath | ConvertFrom-Json
        $workItemTypeFields | ForEach-Object {
            $uri = $global:DeleteFieldUri -f $_.processId, $_.witRefName, $_.fieldRefName
            DeleteContent $uri
        }

        LogMessage -type "Info" -message " Rollback of Fields are done."
    }
}

# DeleteProjects funtion delete's imported Projects
function DeleteProjects {
    if ((Test-Path $global:ProjectResponsePath -PathType Leaf)) {
        $projects = Get-Content $global:ProjectResponsePath | ConvertFrom-Json
        $projects | ForEach-Object {
            $uri = $global:DeleteProjectUri -f $_.newProjectId
            DeleteContent $uri
        }

        LogMessage -type "Info" -message " Rollback of Projects are done."
    }
}

# DeleteProcesses funtion delete's imported Processes
function DeleteProcesses {
    if ((Test-Path $global:ProcessesResponsePath -PathType Leaf)) {
        $processes = Get-Content $global:ProcessesResponsePath | ConvertFrom-Json 
        $processes | ForEach-Object {
            $uri = $global:DeleteProcessUri -f $_.newTypeId
            DeleteContent $uri
        }    

        LogMessage -type "Info" -message " Rollback of Processes are done."
    }
}

