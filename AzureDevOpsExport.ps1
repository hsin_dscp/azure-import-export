# Load the LoggingFunctions.ps1 script
. "$PSScriptRoot\CommanFunctions.ps1"
# Load the ExportGlobalConfigurations.ps1 script
. "$PSScriptRoot\ExportGlobalConfigurations.ps1"
# Load the DevOpsRestApiOperations.ps1 script
. "$PSScriptRoot\DevOpsRestApiOperations.ps1"

# Global Variables
$ProjectFolderName = "Projects"

# Function to handle export of Layout, States, Rules for all WorkItemTypes
function RetrieveLayoutStatesRulesForAllWorkItemTypes {
    param (
        [string]$proccessId,
        [PSCustomObject]$proccessWorkItemTypes
    )

    foreach ($workItemType in $proccessWorkItemTypes.value) {

        if ( $workItemType.customization -ne "system") {

            $referenceName = $workItemType.referenceName
            $uri = $($global:GetLayoutUri -f $proccessId, $referenceName)
            # Get layout for each work item types
            try {
                $response = GetContent $uri 
                $response | ConvertTo-Json -Depth 25 | Out-File -FilePath $($global:GetLayoutFilePath -f $proccessId, $referenceName)
            }
            catch {
                LogMessage -type "Info" -message "No layout present for work item type $referenceName"
            }
            # Get states for each work item types
            try {
                $stateUri = $uri -replace "layout?" , "states"
                $response = GetContent $stateUri 
                $response | ConvertTo-Json -Depth 25 | Out-File -FilePath $($global:GetStatesFilePath -f $proccessId, $referenceName)
            }
            catch {
                LogMessage -type "Info" -message "No states present for work item type $referenceName"
            }
            # Get rules for each work item types
            try {
                $rulesUri = $uri -replace "layout?" , "rules"
                $response = GetContent $rulesUri 
                $response | ConvertTo-Json -Depth 25 | Out-File -FilePath $($global:GetRulesFilePath -f $proccessId, $referenceName)
            }
            catch {
                LogMessage -type "Info" -message "No rules present for work item type $referenceName"
            }

            # Get fields for each work item types
            try {
                $fieldsUri = $uri -replace "layout?" , "fields"
                $response = GetContent $fieldsUri 
                $filteredResponse = $response.value | Where-Object { $_.customization -ne "system" }

                if ($null -ne $filteredResponse) {
                    $fieldResponseArray = New-Object System.Collections.ArrayList
                    $filteredResponse | ForEach-Object {               
                        $fieldResponse = GetContent $($global:GetFieldsUri -f $_.referenceName) 

                        if ($fieldResponse.isPicklist) {
                            $pickListResponse = GetContent $($global:GetPickListUri -f $fieldResponse.picklistId)
                            $fieldResponse | Add-Member -MemberType NoteProperty -Name "pickList" -Value $pickListResponse
                        }

                        $fieldResponse | Add-Member -MemberType NoteProperty -Name "required" -Value $_.required
                        $fieldResponse | Add-Member -MemberType NoteProperty -Name "defaultValue" -Value $_.defaultValue
                        $fieldResponse | Add-Member -MemberType NoteProperty -Name "customization" -Value $_.customization
                        $fieldResponseArray += $fieldResponse
                    }
                    $fieldResponseArray | ConvertTo-Json -Depth 25 | Out-File -FilePath $($global:GetFieldsFilePath -f $proccessId, $referenceName)
                }
            }
            catch {
                LogMessage -type "Info" -message "No fields present for work item type $referenceName"
            }
        }
    }
}
 
# All Work Item Types based on selected processes
function RetrieveAllWorkItemTypesForProccess {
    param (
        [string]$proccessId,
        [string]$proccessName
    ) 
    try {
        LogMessage -type "Info" -message "$proccessName -Retrieving work item types from API..." -errorTrace $null -writeHost $true
        
        $workItemTypeResponse = GetContent $($global:GetWorkItemTypesUri -f $proccessId)
        $workItemTypeResponse | ConvertTo-Json |  Out-File $($global:WorkItemTypesFilePath -f $proccessId, $proccessName) 
        RetrieveLayoutStatesRulesForAllWorkItemTypes $proccessId $workItemTypeResponse
    }
    catch {
        $errorMessage = "An unexpected error occurred while retrieving All Work Item Types."
        ShowErrorMessage -type "Error" -message $errorMessage
        LogException $_
        exit 1
    }
}

# Function to handle export of Dashboards
function RetriveAzureDevOpsDashboardsForEachProject {
    param (
        [string]$projectId
    )

    try { 
        LogMessage -type "Info" -message "Retrieving Dashboards from API..." -errorTrace $null -writeHost $true
        
        $response = GetContent $($global:GetDashboardUri -f $projectId)
        
        # Process the response data here
        # For example, you can save the dashboards to a file or process them further
        if ($null -ne $response) {
            $response | ConvertTo-Json | Out-File -FilePath $($global:DashboardFilePath -f $projectId)
            RetriveDashboardWidgets $projectId $response
        }
        return $true
    }
    catch {
        $errorMessage = "An unexpected error occurred while retrieving dashboards."
        ShowErrorMessage -type "Error" -message $errorMessage
        LogException $_
        exit 1
    }
}

# Function to handle export of Dashboards Widgetd
function  RetriveDashboardWidgets {
    param (
        [string]$projectId,
        [PSCustomObject]$response 
    )
    try {
        LogMessage -type "Info" -message "Retrieving Dashboard Widgets from API..." -errorTrace $null -writeHost $true
        
        $response.value | ForEach-Object {
            $response = GetContent $_.url
            $response | ConvertTo-Json -Depth 25 | Out-File -FilePath $($global:DashboardWidgetsFilePath -f $_.id)
        }
        return $true
    }
    catch {
        $errorMessage = "An unexpected error occurred while retrieving dashboard widgets."
        ShowErrorMessage -type "Error" -message $errorMessage
        LogException $_
        exit 1
    }
}

# Function to handle export of Queries
function RetriveAzureDevOpsQueriesForEachProject { 
    param (
        [string]$projectId
    )
    try {
        
        LogMessage -type "Info" -message "Retrieving queries for projects from API..." -errorTrace $null -writeHost $true
        $projectFilePath = $global:ProjectFilePath
        if (Test-Path $projectFilePath -PathType Leaf) { 
            $projectNames = Get-Content $projectFilePath | ConvertFrom-Json
            
            foreach ($project in $projectNames) { 
                if ($projectId -eq $project.id) {
                    # Get the queries in the specified project
                    $response = GetContent $($global:GetProjectQueriesUri -f $project.name) 
                    $generatedJson = @()
                    foreach ($child in $response.value) { 
                        if ($child.isPublic -ne $false) { 
                            $topLevelObject = @() 
                            if ($child.isFolder -eq $true -and $child.hasChildren -eq $true) {
                                LogMessage -type "Info" -message "Folder has children. Fetching subfolders and queries for '$($child.path)" -errorTrace $null -writeHost $true 
                                $topLevelObject = RetriveAzureDevOpsChildQueries -projectname $project.name -query $child.path 
                            }  
                            $generatedJson += $topLevelObject
                        }
                    }
                    if ($generatedJson.count -ne 0) { 
                        $response.value = $generatedJson
                        $response  | ConvertTo-Json -Depth 25 | Out-File -FilePath $($global:ProjectQuerieFilePath -f $projectId) 
                    }
                    else {
                        $response  | ConvertTo-Json -Depth 25 | Out-File -FilePath $($global:ProjectQuerieFilePath -f $projectId)                         
                    }

                }
            } 
        }
        return $true 
    }
    catch {
        $errorMessage = "An unexpected error occurred while retrieving project queries."
        ShowErrorMessage -type "Error" -message $errorMessage
        LogException $_
        exit 1
    }
}

function RetriveAzureDevOpsChildQueries { 
    param (
        [string]$projectname,
        [string]$query
    )
    try {
        LogMessage -type "Info" -message "Retrieving child queries for projects from API..." -errorTrace $null -writeHost $true
        $childQuery = $global:GetProjectChildQueriesUri -f $projectname, $query
        $result = GetContent $childQuery 
        $newChildren = @()
        foreach ($child in $result.children) {
            if ($child.isFolder -eq $true -and $child.hasChildren -eq $true) {
                LogMessage -type "Info" -message "Folder has children. Fetching subfolders and queries for '$($child.path )'" -errorTrace $null -writeHost $true 
                $newList = RetriveAzureDevOpsChildQueries -projectname $projectname -query $child.path 
                $child | Add-Member -MemberType NoteProperty -Name "children" -Value $newList.children
            } 
            $newChildren += $child
        }

        $result.children = $newChildren
        return $result
    }
    catch {
        $errorMessage = "An unexpected error occurred while retrieving project queries."
        ShowErrorMessage -type "Error" -message $errorMessage
        LogException $_
        exit 1
    }
}


# Function to handle export of Projects
function RetrieveFilterAllProjects {
    param (
        [array]$process_names
    )
    LogMessage -type "Info" -message "Retrieving projects from API..."
    # Retrieve all projects

    try {
        $response = GetContent -uri $global:GetProjectUri
        $global:AllProjects = $response.value
        $ProjectArray = $global:AllProjects

        $processNameProjectMap = @{}

        foreach ($project in $ProjectArray) {
            $projectId = $project.id 
            try {
                $projectDetailResponse = GetContent -uri $($global:GetProjectDetailsUri -f $projectId)
               
                $processTemplateName = $projectDetailResponse.capabilities.processTemplate.templateName

                # Check if the process name contains any of the provided names
                foreach ($process in $process_names) {
                    $processname = $process.name
                    if (-not $processNameProjectMap.ContainsKey($processname)) {
                        $processNameProjectMap[$processname] = @()
                    }
                    if ($processTemplateName -eq $processname) { 
                        $jsonData = $projectDetailResponse | ConvertTo-Json
                        $jsonData | Out-File -FilePath $($global:ProjectDetailsFilePath -f $projectId)
                        $processNameProjectMap[$processname] += $projectId
                    }
                }
            }
            catch {
                LogMessage -type "Error" -message "An unexpected error occurred while processing project '$($project.id)'." -errorTrace $null-writeHost $true
                LogException $_               
                exit 1
            }
        }

        # Prepare the output JSON array
        $outputArray = @()

        foreach ($processName in $processNameProjectMap.Keys) {
            $processObj = @{
                "ProcessName" = $processName
                "ProjectIds"  = $processNameProjectMap[$processName]
            }
            $outputArray += $processObj
        }

        return $outputArray | ConvertTo-Json
    }
    catch {
        LogMessage -type "Error" -message "An unexpected error occurred while retrieving projects." -errorTrace $null -writeHost $true
        LogException $_
        exit 1
    }
}
 

# Function to handle export of Process
function FilterSelectedProcesses { 
    param (
        [array]$process_names
    )
    LogMessage -type "Info" -message "Retrieving processes from API..." -errorTrace $null -writeHost $false
    # Make the API request and save the response to a temporary file
    try {
        $result = GetContent -uri $global:GetProcessesUri -Encoding UTF8
        $jsonFilterProcessObject = New-Object System.Collections.ArrayList
        # Check if each process name from $process_names exists in the response
        if ($process_names -ne "*") {
            foreach ($process in $process_names) {
                $name = $process.Name
                $matchingProcess = $result.value | Where-Object { $_.name -eq $name }
                if ($matchingProcess) {
                    $jsonFilterProcessObject += $matchingProcess
                } 
            }
        }
        else {
            $jsonFilterProcessObject += $result.value
        } 
         
        return $jsonFilterProcessObject
    }
    catch {
        $errorMessage = "An unexpected error occurred while retrieving processes."
        ShowErrorMessage -type "Error" -message $errorMessage
        LogException $_
        exit 1
    }
}

# Function to display processes and projects in a tree-like structure
function ShowTree($processes) {
    try {
        $JsonObjectForMatchingProject = New-Object System.Collections.ArrayList
        $jsonProcessObject = New-Object System.Collections.ArrayList
        $AllProcesses = FilterSelectedProcesses $processes
        $jsonProcessObject += $AllProcesses
        foreach ($process in $processes) {
            $jsonProjectObject = New-Object System.Collections.ArrayList
            if ($process -is [string] -and $process -eq "*") {                  
                $AllProjects = RetrieveFilterAllProjects $jsonProcessObject
                $jsonProjectObject += $AllProjects | ConvertFrom-Json
                $processHasProjects = $jsonProjectObject | Where-Object { $_.ProjectIds.Count -gt 0 }
                foreach ($process in $processHasProjects) {
                    $process_name = $process.ProcessName
                    Write-Host ("+ $process_name") 
                    $currentProjects = $process.ProjectIds
                    foreach ($project in $currentProjects) {
                        $matchProject = $global:AllProjects | Where-Object { $_.id -eq $project } 
                        if ($matchProject) {
                            $JsonObjectForMatchingProject += $matchProject
                            $project_name = $matchProject.name
                            Write-Host "  -  $project_name" 
                        }
                    }  
                } 
            }
            else {
                $processname = $process.Name
                $jsonString = "[{{""Name"": ""{0}""}}]" -f $processname   
                if ($process.Projects -is [string] -and $process.Projects -eq "*") { 
                    # Convert the JSON string to a JSON array
                    $jsonArray = $jsonString | ConvertFrom-Json
                    $AllProjects = RetrieveFilterAllProjects $jsonArray
                    $jsonProjectObject += $AllProjects | ConvertFrom-Json
                    $processHasProjects = $jsonProjectObject | Where-Object { $_.ProjectIds.Count -gt 0 }
                    foreach ($process in $processHasProjects) {
                        $process_name = $process.ProcessName
                        Write-Host ("+ $process_name") 
                        $currentProjects = $process.ProjectIds
                        foreach ($project in $currentProjects) {
                            $matchProject = $global:AllProjects | Where-Object { $_.id -eq $project } 
                            if ($matchProject) {
                                $JsonObjectForMatchingProject += $matchProject
                                $project_name = $matchProject.name
                                Write-Host "  -  $project_name" 
                            }
                        }  
                    } 
                }
                elseif ($process.Projects -is [array] -and $process.Projects.Count -gt 0) {
                    # Convert the JSON string to a JSON array
                    $jsonArray = $jsonString | ConvertFrom-Json
                    $AllProjects = RetrieveFilterAllProjects $jsonArray
                    $jsonProjectObject += $AllProjects  | ConvertFrom-Json
                    if ( $jsonProjectObject.ProjectIds.Count -gt 0) {
                        Write-Host ("+ $processname") 
                    }
                    foreach ($project in $process.Projects) { 
                        $matchProject = $global:AllProjects | Where-Object { $_.name -eq $project.ProjectName }
                        if ($matchProject) {
                            $JsonObjectForMatchingProject += $matchProject
                            Write-Host ("  - " + $project.ProjectName)
                        } 
                    }
                }
            }
        }

        if ($JsonObjectForMatchingProject.Count -eq 0) {
            ShowErrorMessage -type "Error" -message "Please configure the project for running process and start export." 
            exit 1
        }
        # Ask if the user wants to export
        $exportChoice = Read-Host "Do you want to export the processes and projects (Y/N)?"
    
        if ($exportChoice -eq 'Y' -or $exportChoice -eq 'y') {
            if ($null -ne $JsonObjectForMatchingProject) {
                $projectFilePath = $global:ProjectFilePath
                if (Test-Path $projectFilePath -PathType Leaf) { 
                    # Read the existing JSON content from the file
                    $existingJson = Get-Content -Path $projectFilePath -Raw
    
                    # Convert the existing JSON content to a PowerShell object
                    $existingData = $existingJson | ConvertFrom-Json
                }
                
                # Add the new object to the existing data
                $existingData += $JsonObjectForMatchingProject
                
                # Convert the updated data back to JSON format
                $updatedJson = $existingData | ConvertTo-Json -Depth 100

                if ($null -ne $updatedJson) {
    
                    # Write the updated JSON content back to the file
                    $updatedJson | Out-File -FilePath $projectFilePath -Encoding UTF8 

                    $updatedProjectJson = $updatedJson | ConvertFrom-Json

                    foreach ($project in $updatedProjectJson) {
                        $projectId = $project.id 
                        RetriveAzureDevOpsDashboardsForEachProject $projectId
                        RetriveAzureDevOpsQueriesForEachProject $projectId 
                    }
                } 
                else {
                    ShowErrorMessage -type "Error" -message "Please configure the project for running process and start export." 
                    exit 1
                }
            }
            if ($null -ne $jsonProcessObject) { 
                
                $processesFilePath = $global:ProcessesFilePath
                # Convert the $result object to a JSON string
                $jsonString = $jsonProcessObject | ConvertTo-Json -Depth 100 

                # Write the JSON string to the output file
                $jsonString | Set-Content -Path $processesFilePath -Force -Encoding UTF8
                foreach ($process in $jsonProcessObject) { 
                    RetrieveAllWorkItemTypesForProccess $process.typeId $process.name
                }  
            }   
            if ($null -ne $JsonObjectForMatchingProject -and $null -ne $jsonProcessObject) {
                ZipExportFolders
            }
            else {
                DeleteAllExports
            }
        }
        elseif ($exportChoice -eq 'N' -or $exportChoice -eq 'n') {
            DeleteAllExports
        }
        else {
            LogMessage -type "Info" -message "Invalid input. Please enter 'Y' or 'N' only." -s
        }
    }
    catch {
        $errorMessage = "An unexpected error occurred in the ShowTree part of the script." 
        LogMessage -type "Error" -message $errorMessage
        LogException $_
    }
}

# ExportAzureDevOps function do the Export Config Validation and ask for the confimation
function ExportAzureDevOps {  
    
    LogMessage -type "Info" -message "Json Validation..." -errorTrace $null -writeHost $true
     
    $jsonFile = Join-Path -Path $PSScriptRoot -ChildPath "ExportConfig.json"
    $jsonContent = Get-Content -Path $jsonFile -Encoding UTF8
    $jsonKeyFile = Join-Path -Path $PSScriptRoot -ChildPath "RequiredKeys.json"
    $jsonRequiredKeyconfig = Get-Content -Path $jsonKeyFile | ConvertFrom-Json
    $exportJsonString = $jsonRequiredKeyconfig.Keys.Export  | ConvertTo-Json
    $isValidJson = ValidateJson -jsonString $jsonContent $exportJsonString

    if ($isValidJson) { 
        if (-Not (Test-Path $global:ExportFolderPath -PathType Container)) {
            New-Item -ItemType Directory -Path $global:ExportFolderPath | Out-Null 
            LogMessage -type "Info" -message "Folder recreated: $global:ExportFolderPath" -errorTrace $null -writeHost $false
        }
        else { 
            Remove-Item -Path $global:ExportFolderPath -Force -Recurse
            New-Item -ItemType Directory -Path $global:ExportFolderPath | Out-Null 
            LogMessage -type "Info" -message "Folder recreated: $global:ExportFolderPath" -errorTrace $null -writeHost $false
        } 
        # List of folders to be created
        $foldersToCreate = @( 
            "Dashboards",
            "Dashboards\Widgets",            
            "Processes",
            $ProjectFolderName,
            "Queries", 
            "ProcessWorkItemTypes",
            "ProcessWorkItemTypes\WorkItemTypeLayout",
            "ProcessWorkItemTypes\WorkItemTypeStates",
            "ProcessWorkItemTypes\WorkItemTypeStates",
            "ProcessWorkItemTypes\WorkItemTypeRules",            
            "ProcessWorkItemTypes\WorkItemTypeFields",            
            "ProjectDetails",
            "Organization"
        ) 
        # Loop through each folder and create if it doesn't exist
        foreach ($folder in $foldersToCreate) {
            $folderPath = Join-Path -Path $global:ExportFolderPath -ChildPath $folder
            if (-Not (Test-Path $folderPath -PathType Container)) {
                New-Item -ItemType Directory -Path $folderPath | Out-Null
                $logMessage = "Folder created: $folderPath" 
                LogMessage -type "Info" -message $logMessage 
            }
            else {
                $logMessage = "Folder already exists: $folderPath" 
                LogMessage -type "Info" -message $logMessage  
            }
        }
        $jsonExportObject = $jsonContent | ConvertFrom-Json
        ShowTree $jsonExportObject.Processes
    }
    else {
        # Stop further processing or display an error message 
        $errorMessage = "Export configuration is not valid. Please provide valid configuration."
        ShowErrorMessage -type "Error" -message $errorMessage
        exit 1
    }  
}

# Function Start the export
function StartExport {
    try {
        # Assign variables from the JSON object and rest api urls as global variables 
        InitializeGlobalFields       
        LogMessage -type "Info" -message "Welcome to the Azure Export Data Management Tool!" -errorTrace $null -writeHost $true

        LogMessage -type "Info" -message "Script started."

        ExportAzureDevOps

        LogMessage -type "Info" -message  "Script finished."
    }
    catch {
        LogMessage -type "Error" -message "An unexpected error occurred in the main part of the script." -errorTrace $null -writeHost $true  
        LogException $_
    }
}

# Main Part
StartExport
