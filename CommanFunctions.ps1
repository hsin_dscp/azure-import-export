
# LogException function logs the complete exception details
function LogException {
    param (
        [PSCustomObject]$exception        
    )
    
    $logFilePath = GetLogFilePath

    Write-Host " "
    $design = "------------------------- Exception Details ---------------------------------"
    Write-Host $design -ForegroundColor Yellow
    $design | Out-File -Append -FilePath $logFilePath
    if ($null -ne $exception.Exception) {
        $message = $exception.Exception.Message
        Write-Host " Message : $($message)" -ForegroundColor Red
        $message | Out-File -Append -FilePath $logFilePath
        if ($null -ne $exception.ErrorDetails) {
            $message = $exception.ErrorDetails.Message
            Write-Host " ErrorDetails.Message : $message ." -ForegroundColor Red
            $message | Out-File -Append -FilePath $logFilePath
        }

        if ($null -ne $exception.Exception.StackTrace) {
            $sTrace = $exception.Exception.StackTrace
            #Write-Host " StackTrace : $sTrace" -ForegroundColor Red
            $sTrace | Out-File -Append -FilePath $logFilePath
        }

        if ($null -ne $exception.Exception.InnerException) {
            $innerException = $exception.Exception.InnerException
            Write-Host " InnerException : $innerException" -ForegroundColor Red
            $innerException | Out-File -Append -FilePath $logFilePath
        }        
    }
    $design = "------------------------- ----------------- ---------------------------------"
    Write-Host  $design -ForegroundColor Yellow
    $design | Out-File -Append -FilePath $logFilePath
    Write-Host " "
    exit 1
}

#LogRequestDetailsOnException function log the API request details
function LogRequestDetailsOnException {
    param (
        [string]$uri,
        [PSCustomObject]$requestBody
    )
    
    $logFilePath = GetLogFilePath
    Write-Host " "
    $dataStr = "------------------------- Failed Request Details ---------------------------------"
    Write-Host $dataStr -ForegroundColor Yellow
    $dataStr | Out-File -Append -FilePath $logFilePath
    $dataStr = " Request URL : $uri"
    Write-Host $dataStr -ForegroundColor Red
    $dataStr | Out-File -Append -FilePath $logFilePath
    if ($null -ne $requestBody) {
        $dataStr = $requestBody | ConvertTo-Json -Depth 25
        $dataStr = " Request Body JSON : $dataStr"
        Write-Host $dataStr -ForegroundColor Red
        $dataStr| Out-File -Append -FilePath $logFilePath
    }
    $dataStr = "------------------------- ----------------- ---------------------------------"
    Write-Host $dataStr -ForegroundColor Yellow
    $dataStr | Out-File -Append -FilePath $logFilePath
}

# LogMessage function logs the message
function LogMessage {
    param (
        [string]$type,
        [string]$message,
        [string]$errorTrace = $null,
        [bool]$writeHost = $false
    )
    if ($type -eq "Info") {
        $logEntryString = " DateTime: {0}, Type: {1}, Message: {2}" -f (Get-Date -Format "yyyy-MM-dd HH:mm:ss"), $type, $message 
    }
    else {
        $logEntryString = " DateTime: {0}, Type: {1}, Message: {2}, StackTrace: {3}" -f (Get-Date -Format "yyyy-MM-dd HH:mm:ss"), $type, $message, $errorTrace       
    } 

    $logFilePath = GetLogFilePath

    if ($writeHost -eq $true) {
        $logEntryString = " DateTime: {0}, Type: {1}, Message: {2} " -f (Get-Date -Format "yyyy-MM-dd HH:mm:ss"), $type, $message
        Write-Host $logEntryString
    }
    $logEntryString | Out-File -Append -FilePath $logFilePath
}

function LogExportConfig {
    $logFilePath = GetExportConfigLogPath
    $logEntry  = @{
        ExportOrgUrl = "$global:BaseAPIUrl/$global:OrganizationName"
    }
    $logEntryString =  $logEntry | ConvertTo-Json
    $logEntryString | Out-File -Append -FilePath $logFilePath
}

# ShowErrorMessage function logs and show error message
function ShowErrorMessage {
    param (
        [string]$type,
        [string]$message
    )

    $logFilePath = GetLogFilePath
    $logEntryString = " DateTime: {0}, Type: {1}, Message: {2} " -f (Get-Date -Format "yyyy-MM-dd HH:mm:ss"), $type, $message
    Write-Host $logEntryString -ForegroundColor Red
    $logEntryString | Out-File -Append -FilePath $logFilePath
}

# This function returns log file path
function GetLogFilePath {
    $logFilePath = ""
    if ($null -ne $global:ImportErrorFilePath) {
        $logFilePath = "$PSScriptRoot/$global:ImportErrorFilePath" 
    }
    else {
        $logFilePath = "$PSScriptRoot/$global:ExportErrorPath" 
    }
    
    return $logFilePath
}

# This function returns Export Config Path file path
function GetExportConfigLogPath 
{
    $logFilePath = ""
    $logFilePath = "$global:ExportFolderPath/Organization/$global:ExportConfigPath"  
    return $logFilePath
}

#Show loader function shows the loader
function Show-Loader {
    param(
        [int]$totalSteps,
        [int]$delayMilliseconds
    )

    $progressWidth = 50
    $percentFormat = "{0,3}%"
    $percentChar = "%"

    for ($step = 1; $step -le $totalSteps; $step++) {
        # Calculate the percentage completed
        $percentComplete = ($step / $totalSteps) * 100

        # Calculate the number of '#' characters to display
        $numChars = [math]::Round(($percentComplete / 100) * $progressWidth)
        $progressBar = '#' * $numChars + ' ' * ($progressWidth - $numChars)

        # Display the progress bar and percentage
        Write-Host -NoNewline ("[{0}] " -f $progressBar)
        Write-Host -NoNewline ($percentFormat -f $percentComplete)
        Write-Host -NoNewline $percentChar

        # Sleep to simulate the loading time
        Start-Sleep -Milliseconds $delayMilliseconds

        # Move the cursor back to the beginning of the line
        Write-Host -NoNewline "`r"
    }
 
} 

# ValidateJson validate the export and import configuration file for corect entry
function ValidateJson {
    param (
        [string]$jsonString,
        [string]$requiredKeyconfigString
    ) 
    $missingKeys = @()
    $logMessage = " Validating for configuration file is started..."
    Write-Host $logMessage
    LogMessage -type "Info" -message $logMessage
    $JsonValid = $true
    try {
        $jsonObject = $jsonString | ConvertFrom-Json
        $requiredKeyconfig = $requiredKeyconfigString | ConvertFrom-Json
    }
    catch {
        $errorMessage = "Invalid JSON format: $_"
        ShowErrorMessage -type "Error" -message $errorMessage 
        LogException $_
        $JsonValid = $false
    }
    
    # Compare the two PowerShell objects for an exact match 
    $ExportJsonFormatKeys = $jsonObject.PSObject.Properties.Name 
    foreach ($requiredKey in $requiredKeyconfig) {
        if ($requiredKey -notin $ExportJsonFormatKeys) {
            $JsonValid = $false
            $missingKeys += $requiredKey
        }
        else {
            Write-Host " Validation is completed for  $requiredKey"
        }
    }  
    if ($JsonValid) {
        # Check if any key has an empty value
        $emptyKeys = $jsonObject.PSObject.Properties | Where-Object { [string]::IsNullOrEmpty($_.Value) } | Select-Object -ExpandProperty Name

        if ($emptyKeys.Count -gt 0 -and $emptyKeys -ne "Processes") {
            ShowErrorMessage -type "Error" -message "Empty values for keys: $($emptyKeys -join ', ')"
            $JsonValid = $false
        }
        elseif ($jsonObject.Processes -ne "*") {
            if ($null -ne $jsonObject.Processes -and $jsonObject.Processes -is [array] -and $jsonObject.Processes.Count -gt 0) {
                foreach ($item in $jsonObject.Processes) {
                    foreach ($property in $item.PSObject.Properties) {
                        if ( $null -eq $property.Value -or $property.Value -eq '') { 
                            ShowErrorMessage -type "Error" -message "Variable for key '$($property.Name)' within an object in 'Processes' is empty or contains no value."
                            $JsonValid = $false
                        }                      
                    }
                    foreach ($project in $item.Projects) {
                        foreach ($property in $project.PSObject.Properties) {
                            if ( $null -eq $property.Value -or $property.Value -eq '') { 
                                ShowErrorMessage -type "Error" -message "Variable for key '$($property.Name)' within an object in 'Project' is empty or contains no value."
                                $JsonValid = $false
                            }
                        }
                    } 
                }  
            } 
            else { 
                $JsonValid = $false
            }
        }
        else {
            if ($null -ne $jsonObject.Processes) {
                $JsonValid = $true
            }
            else {
                $JsonValid = $false
            }
        } 
    }

    if ($JsonValid) {
        $logMessage = " Validation for configuration file is completed successfully..."
    }
    else {
       
        if ($missingKeys.Count -gt 0 ) {
            if ($missingKeys.Count -eq 1 ) {
                $logMessage = " Key is missing in config file: $($missingKeys -join ', ')"
            }
            else
            {
                $logMessage = " Keys are missing in config file: $($missingKeys -join ', ')"
            }
            ShowErrorMessage -type "Error" -message $logMessage 
        }
        $logMessage = " Validation for configuration file is completed unsuccessfully..."
    }
    Write-Host $logMessage
    LogMessage -type "Info" -message $logMessage

    return $JsonValid
}

# ZipExportFolders function zip all the exported data
function ZipExportFolders {
    try { 
        $logMessage = "Creating Zip file..."
        LogExportConfig
        Write-Host $logMessage
        LogMessage -type "Info" -message $logMessage
        $ZipFilePath = Join-Path -Path $global:ExportFolderPath -ChildPath $global:ExportFileName
        Compress-Archive -Path "$global:ExportFolderPath/Dashboards", "$global:ExportFolderPath/Processes", "$global:ExportFolderPath/Projects", "$global:ExportFolderPath/Queries",  "$global:ExportFolderPath/ProcessWorkItemTypes", "$global:ExportFolderPath/ProjectDetails", "$global:ExportFolderPath/Organization" -DestinationPath $ZipFilePath -Force
        # Check if the compression was successful
        if ($?) {  
            $logMessage = " ZIP file created successfully."
            Write-Host $logMessage
            LogMessage -type "Info" -message $logMessage
            DeleteAllExports
        }
        else {
            $errorMessage = " Failed to create the ZIP file."
            Write-Error $errorMessage
            LogMessage -type "Error" -message $errorMessage -stackTrace $_.Exception.StackTrace
            return $false 
        }
    }
    catch {
        $errorMessage = "Error occurred while creating zip files projects. Error: $_"
        Write-Error $errorMessage
        LogMessage -type "Error" -message $errorMessage -stackTrace $_.Exception.StackTrace
        return $false  
    } 
}

# DeleteAllExports function deletes all the exported folders after zip
function DeleteAllExports {
    try {
        if ((Test-Path -Path $global:ExportFolderPath -PathType Container)) {
            Remove-Item -Path "$global:ExportFolderPath/Dashboards", "$global:ExportFolderPath/Processes", "$global:ExportFolderPath/Projects", "$global:ExportFolderPath/Queries",  "$global:ExportFolderPath/ProcessWorkItemTypes", "$global:ExportFolderPath/ProjectDetails", "$global:ExportFolderPath/Organization" -Recurse
        }
    }

    catch {
        $errorMessage = "Error occurred while creating zip files projects. Error: $_"
        Write-Error $errorMessage
        LogMessage -type "Error" -message $errorMessage -stackTrace $_.Exception.StackTrace
        return $false 
    }    
}

# DeleteImportPath function deletes all the unzipped folders 
function DeleteImportPath {
    try {
        if ((Test-Path -Path $global:ImportFolderPath -PathType Container)) {
            Remove-Item -Path $global:ImportFolderPath -Recurse
        }
    }
    catch {
        $errorMessage = "Error occurred while Deleting Import Path. Error: $_"
        Write-Error $errorMessage
        LogMessage -type "Error" -message $errorMessage -stackTrace $_.Exception.StackTrace
        return $false 
    }    
}

# CheckIsPresenceInTarget function check the presense of resource in target organization return it
function CheckIsPresenceInTarget {
    param(
        [string]$uri
    )

    try {
        $results = Invoke-RestMethod -Uri $uri -Headers @{Authorization = "Basic $global:ApiKey" } -Method Get -ErrorAction Stop
    }
    catch {
        return $null
    }

    return $results
}

# IsResourcePresent function check the presense of resource in target organization
function IsResourcePresent {
    param(
        [string]$uri
    )
    try { $results = Invoke-RestMethod -Uri $uri -Headers @{Authorization = "Basic $global:ApiKey" } }
    catch {}
    return ($null -ne $results) 
} 

# This function moves export .zip file to the archive folder
function ArchiveFile {
   
    try {
        $pathWithCurrentDateTime = "Archive_$(Get-Date -Format "_yyyyMMdd_HHmmss")"
        $archivePath = $global:ArchiveFolderPath + "\" + $pathWithCurrentDateTime
        # Create the archive folder if it doesn't exist
        if (-not (Test-Path -Path $archivePath)) {
            New-Item -Path $archivePath -ItemType Directory
        }

        # Move the source ZIP file to the archive folder
        Move-Item -Path $($global:ImportZipFilePath + "\" + $global:ImportZipFileName)-Destination $archivePath
        LogMessage -type "Info" -message "$global:ImportZipFilePath file has been moved to archive $archivePath path"
    }
    catch {
        ShowErrorMessage -type "Error" -message "An error has been occured while archiving $global:ImportZipFilePath file to $archivePath path"
    }
    
}