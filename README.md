# Azure DevOps Data Export Tool - README

## Technical Overview

The "Azure DevOps Data Export Tool" is a PowerShell script designed to facilitate data export from your Azure DevOps organization. This tool automates the process of selecting and exporting specific processes, projects, and their associated work item types to a ZIP archive, providing users with an efficient way to manage their organization's data.

### Process Flow

The "Azure DevOps Data Export Tool" follows a step-by-step process to efficiently export data from your Azure DevOps organization:

1. **Initialization**: The script starts by initializing the global variables, setting the base URL for the Azure DevOps API, and defining necessary API versions.

2. **Folder Creation**: The script checks for the existence of the following folders and creates them if they don't already exist:

   - `Dashboards`: For exporting dashboards (if any).
   - `Process`: For storing process-related data.
   - `Projects`: To save project details.
   - `Queries`: For exporting queries (if any).
   - `ProcessWorkItemTypes`: To store the details of work item types based on the selected processes.
   - `ProjectDetails`: To store detailed information about the selected projects.

3. **Process and Work Item Type Selection**: The script retrieves available processes from the Azure DevOps organization by making an API call to `/_apis/work/processes`. It then presents the processes to the user and allows them to select the processes they want to export. It also fetches the associated work item types for each selected process by calling `RetrieveAllWorkItemTypesForProccess`.

4. **Work Item Type Retrieval**: For each selected process, the script calls the `RetrieveAllWorkItemTypesForProccess` function. This function makes an API call to `/work/processes/{processId}/workitemtypes` to fetch all the work item types associated with that process. The details of each work item type are saved in a separate JSON file.

5. **Project Retrieval and Filtering**: The script retrieves all projects in the Azure DevOps organization by making an API call to `/_apis/projects`. It stores the details of each project in separate JSON files located inside the "ProjectDetails" folder. Subsequently, the script filters the projects based on the selected processes. Only projects that use any of the selected processes will be included for export, while others are excluded.

6. **Data Export and Compression**: After project filtering, the script has all the necessary data (projects, work item types, etc.) to be exported. It has already stored this data in separate JSON files.

7. **Data Compression**: The script uses the `Compress-Archive` cmdlet to compress the exported data into a ZIP archive. The ZIP archive is named `org-name-AzureDevopsExport.zip`, where `org-name` is the name of your Azure DevOps organization. The ZIP archive contains all the exported data organized into folders representing each entity (e.g., "Dashboards," "Process," "Projects," etc.).

8. **Cleanup**: Finally, the script cleans up the temporary folders used for storing exported data. It clears the content of the folders "Dashboards," "Process," "Projects," "Queries," "ProcessWorkItemTypes," and "ProjectDetails."

9. **Completion**: The data export process is now complete. The user will find the ZIP archive (`org-name-AzureDevopsExport.zip`) containing the exported data in the same location as the script.

### Getting Started

To use the "Azure DevOps Data Export Tool," follow the steps below:

1. **Prerequisites**: Ensure you have PowerShell installed on your system.

2. **Clone the Repository**: Clone or download the repository to your local machine.

3. **Configuration**: Open the script file (`AzureDevOpsDataExport.ps1`) and set the necessary global variables at the beginning of the script:
  
    - `$organization_name`: Replace this variable with your Azure DevOps organization name.
    - `$API_KEY`: Replace this variable with your Azure DevOps personal access token for authentication.
    - `$selected_project_ids`: An array to store the selected project IDs based on the chosen processes.
    - `$selected_process_names`: An array to store the names of the selected processes.
    - `$global:work_item_type_names`: An array to store work item type names associated with the selected processes.
    - `$ProjectFolderName`: Name of the folder where project details will be stored.
    - `$ZipPath`: Path for the ZIP archive that will contain the exported data.
    - `$apiVersion_6_0` and `$apiVersion_7_0`: API versions used for the Azure DevOps API requests.
    - `$baseURL`: The base URL for the Azure DevOps API. 

   Note: The script uses Azure DevOps REST API to interact with your organization, so ensure that you have the appropriate permissions and the API key is valid.

4. **Running the Script**: Open PowerShell, navigate to the folder containing the script, and run the script:

```powershell
.\AzureDevOpsDataExport.ps1
```

These global variables define essential configuration settings for the script. Replace `"org-name"` and `"org-api-token"` with your Azure DevOps organization name and personal access token, respectively. The script will use these values for authentication and accessing the organization's data.

```powershell
# Function to show options for Import and Export
function ExportAzureDevOps { 
    # ...
}
```

This is the main function of the script. It serves as the entry point and orchestrates the entire data export process. The function does the following:

1. **Folder Creation**: Creates the necessary folders to organize the exported data, such as "Dashboards," "Process," "Projects," etc.

2. **Process and Work Item Type Selection**: Retrieves available processes from the Azure DevOps organization and prompts the user to select the processes they want to export. It also fetches the associated work item types for each selected process.

3. **Project Retrieval and Filtering**: Retrieves all projects in the organization and filters them based on the selected processes. Only projects that use any of the selected processes will be included for export.

4. **Data Export and Compression**: Exports the selected data (projects, work item types, etc.) into separate JSON files for each entity. Once the data extraction is complete, it compresses the exported data into a ZIP archive named `org-name-AzureDevopsExport.zip`.

5. **Cleanup**: Clears the content of the temporary folders used for storing exported data.

```powershell
function FilterSelectedProcesses {
    # ...
}
```

This function retrieves available processes from the Azure DevOps organization by making an API call to `/_apis/work/processes`. It presents a list of processes to the user and allows them to select the processes they want to export. It then calls `RetrieveAllWorkItemTypesForProccess` for each selected process to fetch associated work item types.

```powershell
function RetrieveAllWorkItemTypesForProccess {
    param (
        [string[]]$proccess_id,
        [string[]]$proccess_name
    ) 
    # ...
}
```

This function retrieves all work item types for a selected process by making an API call to `/work/processes/{processId}/workitemtypes`. It saves the details of each work item type in a separate JSON file.

```powershell
function RetrieveFilterAllProjectsAsPerProcess {
    # ...
}
```

This function retrieves all projects in the Azure DevOps organization by making an API call to `/_apis/projects`. It stores the details of each project in separate JSON files located inside the "ProjectDetails" folder. Subsequently, it filters the projects based on the selected processes. Only projects that use any of the selected processes will be included for export.

```powershell
function Show-Loader {
    param(
        [int]$totalSteps,
        [int]$delayMilliseconds
    )

    # ...
}
```

This helper function displays a simple loading progress bar during API requests to provide a visual representation of the progress of API calls.

```powershell
function ZipExportFolders {
    # ...
}
```

This function compresses the exported data into a ZIP archive using the `Compress-Archive` cmdlet. It creates the ZIP archive named `org-name-AzureDevopsExport.zip`, where `org-name` is the name of your Azure DevOps organization. The ZIP archive contains all the exported data organized into folders representing each entity.

```powershell
# Main Part
Write-Host "Welcome to the Organization Data Management Tool!"
ExportAzureDevOps
```

The script starts its execution here. It displays a welcome message and calls the `ExportAzureDevOps` function, which initiates the data export process.




### Conclusion

The "Azure DevOps Data Export Tool" simplifies the process of exporting specific processes, projects, and work item types from your Azure DevOps organization. By following the process flow, the script provides an automated and organized way to manage and back up essential data. Users can easily customize the export by selecting the processes they need, ensuring a smooth and efficient data export experience.

Feel free to explore the script, contribute to the project, or suggest improvements to enhance its functionality and usability. Happy data exporting!