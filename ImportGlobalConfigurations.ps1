function InitializeGlobalFields() {
    #Import configs
    $currentDateTime = Get-Date -Format "_yyyyMMdd_HHmmss"
    $importconfigFile = Join-Path -Path $PSScriptRoot -ChildPath "ImportConfig.json"
    $config = Get-Content $importconfigFile -Encoding UTF8 | ConvertFrom-Json
    $global:BaseAPIUrl = $config.ImportBaseAPIUrl
    $global:OrganizationName = $config.ImportOrganizationName
    $parsonalAccessToken = $config.ImportOrganizationAPIKey    
    $global:ApiKey = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(":$parsonalAccessToken"))
    $global:ImportFolderPath = $config.ImportFolderPath
    $global:ImportZipFilePath = $config.ImportZipFilePath
    $global:ImportZipFileName = $config.ImportZipFileName
    $global:ImportErrorFilePath = $($config.ImportErrorFilePath -f $currentDateTime)
    $global:ArchiveFolderPath = $config.ArchiveFolderPath
    $global:ConfigProcesses = $config.Processes
    $global:ExportedProcesses = $null
    $global:WorkItemTypeFields = $null

    #Org Config
    $global:ExportOrgConfigFilePath = "$global:ImportFolderPath\Organization\ExportConfig.json"
    #process
    $global:ProcessesFilePath = "$global:ImportFolderPath\Processes\Processes_Type.json"
    $global:CreateProcessesUri = "$global:BaseAPIUrl/$global:OrganizationName/_apis/work/processes?api-version=7.0"
    $global:ProcessesResponsePath = "$global:ImportFolderPath\Processes\ProcessesTypeResponse\processes_type_response.json"

    #project
    $global:ProjectFilePath = "$global:ImportFolderPath\Projects\Projects.json"
    $global:CreateProjectUri = "$global:BaseAPIUrl/$global:OrganizationName/_apis/projects?api-version=7.0"
    $global:ProjectResponsePath = "$global:ImportFolderPath\Projects\ProjectResponse\import_project_response.json"
    $global:ProjectDetailsFilePath = "$global:ImportFolderPath\ProjectDetails\Project_Details_{0}.json"
    $global:GetProjectUri = "$global:BaseAPIUrl/$global:OrganizationName/_apis/projects/{0}?api-version=7.0"

    #ProccessWorkItemType
    $global:WorkItemTypeFilePath = "$global:ImportFolderPath\ProcessWorkItemTypes\Work_Item_Types_{0}_{1}.json"
    $global:CreateWorkItemTypeUri = "$global:BaseAPIUrl/$global:OrganizationName/_apis/work/processes/{0}/workitemtypes?api-version=7.0"
    $global:WorkItemTypeLayoutFilePath = "$global:ImportFolderPath\ProcessWorkItemTypes\WorkItemTypeLayout\WorkItemTypeLayout_{0}_{1}.json"

    #ImportWorkItemTypeFields
    $global:WorkItemTypeFieldsFilePath = "$global:ImportFolderPath\ProcessWorkItemTypes\WorkItemTypeFields\WorkItemTypeFields_{0}_{1}.json"
    $global:WorkItemTypeFieldsUri = "$global:BaseAPIUrl/$global:OrganizationName/_apis/work/processes/{0}/workItemTypes/{1}/fields?api-version=7.0"
    $global:ProcessFieldsUri = "$global:BaseAPIUrl/$global:OrganizationName/_apis/wit/fields?api-version=7.1-preview.2"
    $global:WorkItemTypeFieldsResponsePath = "$global:ImportFolderPath\ProcessWorkItemTypes\WorkItemTypeFields\FieldsResponse\FieldsResponse_{0}.json"

    #ImportWorkItemTypePage
    $global:WorkItemTypePageUri = "$global:BaseAPIUrl/$global:OrganizationName/_apis/work/processes/{0}/workItemTypes/{1}?api-version=7.0"
    $global:CreateWorkItemTypePageUri = "$global:BaseAPIUrl/$global:OrganizationName/_apis/work/processes/{0}/workItemTypes/{1}/layout/pages?api-version=7.0"

    #ImportWorkItemTypeGroup
    $global:CreateWorkItemTypeGroupUri = "$global:BaseAPIUrl/$global:OrganizationName/_apis/work/processes/{0}/workItemTypes/{1}/layout/pages/{2}/sections/{3}/groups?api-version=7.0"
    $global:UpdateWorkItemTypeGroupUri = "$global:BaseAPIUrl/$global:OrganizationName/_apis/work/processes/{0}/workItemTypes/{1}/layout/pages/{2}/sections/{3}/groups/{4}?api-version=7.0"

    #ImportWorkItemTypeControl
    $global:CreateWorkItemTypeControlUri = "$global:BaseAPIUrl/$global:OrganizationName/_apis/work/processes/{0}/workItemTypes/{1}/layout/groups/{2}/controls?api-version=7.0"
    $global:UpdateWorkItemTypeControlUri = "$global:BaseAPIUrl/$global:OrganizationName/_apis/work/processes/{0}/workItemTypes/{1}/layout/groups/{2}/controls/{3}?api-version=7.0"
    #ImportHtmlFieldControl
    $global:CreateWorkItemTypeHtmlFieldControlUri = "$global:BaseAPIUrl/$global:OrganizationName/_apis/work/processes/{0}/workItemTypes/{1}/layout/pages/{2}?api-version=7.1-preview.1"

    #UpdateAzureDevopsProcessName
    $global:RenameAzureDevopsProcessUri = "$global:BaseAPIUrl/$global:OrganizationName/_apis/work/processes/{0}?api-version=7.1-preview.2"

    #UpdateAzureDevopsProjectName
    $global:RenameDestAzureDevopsProjectUri = "$global:BaseAPIUrl/$global:OrganizationName/_apis/projects/{0}?api-version=7.2-preview.4"

    #ImportDashboardWithWidgets
    $global:GetDashboardsFilePath = "$global:ImportFolderPath\Dashboards\Dashboards_{0}.json"
    $global:CreateDashboardUrl = "$global:BaseAPIUrl/$global:OrganizationName/{0}/_apis/dashboard/dashboards?api-version=7.1-preview.3"
    $global:CreateTeamDashboardUrl = "$global:BaseAPIUrl/$global:OrganizationName/{0}/{1}/_apis/dashboard/dashboards?api-version=7.1-preview.3"
    $global:GetDashboardWidgetsFilePath =  "$global:ImportFolderPath\Dashboards\Widgets\Widgets_{0}.json"
    $global:UpdateDashboardUri = "$global:BaseAPIUrl/$global:OrganizationName/{0}/{1}/_apis/Dashboard/Dashboards/{2}?api-version=7.0-preview.3"

    #ImportQueries
    $global:QueryResponsePath = "$global:ImportFolderPath\Queries\QueriesResponse\QueriesResponse.json"
    $global:CreateQueryUri = "$global:BaseAPIUrl/$global:OrganizationName/{0}/_apis/wit/queries/{1}?api-version=7.0"
    $global:QueryFilePath = "$global:ImportFolderPath\Queries\Queries_{0}.json"

    #ImportWorkItemTypeStates
    $global:WorkItemTypeStatesFilePath = "$global:ImportFolderPath\ProcessWorkItemTypes\WorkItemTypeStates\WorkItemTypeStates_{0}_{1}.json"
    $global:WorkItemTypeStatesUri = "$global:BaseAPIUrl/$global:OrganizationName/_apis/work/processes/{0}/workItemTypes/{1}/states?api-version=7.0"

    #ImportWorkItemTypeRules
    $global:WorkItemTypeRulesFilePath =  "$global:ImportFolderPath\ProcessWorkItemTypes\WorkItemTypeRules\WorkItemTypeRules_{0}_{1}.json"
    $global:WorkItemTypeRulesUri = "$global:BaseAPIUrl/$global:OrganizationName/_apis/work/processes/{0}/workItemTypes/{1}/rules?api-version=7.0"

    #Team
    $global:GetTeamsUri = "$global:BaseAPIUrl/$global:OrganizationName/_apis/teams?api-version=7.0-preview.3"

    #RollBack
    $global:DeleteProjectUri = "$global:BaseAPIUrl/$global:OrganizationName/_apis/projects/{0}?api-version=7.0"
    $global:DeleteProcessUri = "$global:BaseAPIUrl/$global:OrganizationName/_apis/work/processes/{0}?api-version=7.0"
    $global:DeleteFieldUri   = "$global:BaseAPIUrl/$global:OrganizationName/_apis/work/processes/{0}/workItemTypes/{1}/fields/{2}?api-version=7.0"

    #Repository
    $global:GetRepositoryUri   = "$global:BaseAPIUrl/$global:OrganizationName/{0}/_apis/git/repositories?api-version=7.0"

    #ImportPickList
    $global:GetPickListUri   = "$global:BaseAPIUrl/$global:OrganizationName/_apis/work/processes/lists?api-version=7.2-preview.1"

     # ShowConfigurationDetails function logs import configuration details
    function ShowConfigurationDetails {
        Write-Host "ApiKey : $global:ApiKey"
        Write-Host "ConfigProcesses : $global:ConfigProcesses"
        Write-Host "BaseAPIUrl : $global:BaseAPIUrl"
        Write-Host "OrganizationName : $global:OrganizationName"
        Write-Host "ImportFolderPath : $global:ImportFolderPath"
        Write-Host "ImportZipFilePath : $global:ImportZipFilePath"
        Write-Host "ImportZipFileName : $global:ImportZipFileName"
        Write-Host "ArchiveFolderPath : $global:ArchiveFolderPath"
        Write-Host "ExportedProcesses : $global:ExportedProcesses"
        Write-Host "WorkItemTypeFields : $global:WorkItemTypeFields"
        Write-Host "ImportErrorFilePath : $global:ImportErrorFilePath"        
        Write-Host "WorkItemTypeFieldsResponsePath : $global:WorkItemTypeFieldsResponsePath"

        #----------------------------- ---------------------------------#
        #org
        Write-Host "ExportOrgConfigFilePath : $global:ExportOrgConfigFilePath"
        #process
        Write-Host "GetTeamsUri : $global:GetTeamsUri"
        Write-Host "QueryFilePath : $global:QueryFilePath"
        Write-Host "GetProjectUri : $global:GetProjectUri"
        Write-Host "CreateQueryUri : $global:CreateQueryUri"
        Write-Host "ProjectFilePath : $global:ProjectFilePath"
        Write-Host "CreateProjectUri : $global:CreateProjectUri"
        Write-Host "ProjectFilePath : $global:UpdateDashboardUri"
        Write-Host "ProcessesFilePath : $global:ProcessesFilePath"
        Write-Host "QueryResponsePath : $global:QueryResponsePath" 
        Write-Host "CreateProcessesUri : $global:CreateProcessesUri"
        Write-Host "CreateDashboardUrl : $global:CreateDashboardUrl"
        Write-Host "ProjectResponsePath : $global:ProjectResponsePath"
        Write-Host "WorkItemTypePageUri : $global:WorkItemTypePageUri"
        Write-Host "WorkItemTypeFilePath : $global:WorkItemTypeFilePath"
        Write-Host "GetDashboardsFilePath : $global:GetDashboardsFilePath"
        Write-Host "CreateWorkItemTypeUri : $global:CreateWorkItemTypeUri"        
        Write-Host "WorkItemTypeFieldsUri : $global:WorkItemTypeFieldsUri"
        Write-Host "ProcessesResponsePath : $global:ProcessesResponsePath"
        Write-Host "ProjectDetailsFilePath : $global:ProjectDetailsFilePath"
        Write-Host "ProcessFieldsUri : $global:ProcessFieldsUri"
        Write-Host "CreateWorkItemTypeGroupUri : $global:CreateWorkItemTypeGroupUri"
        Write-Host "RenameAzureDevopsProcessUri : $global:RenameAzureDevopsProcessUri"
        Write-Host "RenameDestAzureDevopsProjectUri : $global:RenameDestAzureDevopsProjectUri"
        Write-Host "CreateWorkItemTypePageUri : $global:CreateWorkItemTypePageUri"
        Write-Host "WorkItemTypeLayoutFilePath : $global:WorkItemTypeLayoutFilePath"
        Write-Host "WorkItemTypeFieldsFilePath : $global:WorkItemTypeFieldsFilePath"
        Write-Host "GetDashboardWidgetsFilePath : $global:GetDashboardWidgetsFilePath"
        Write-Host "CreateWorkItemTypeHtmlFieldControlUri : $global:CreateWorkItemTypeHtmlFieldControlUri"      
        Write-Host "WorkItemTypeStatesFilePath :$global:WorkItemTypeStatesFilePath" 
        Write-Host "WorkItemTypeStatesUri :$global:WorkItemTypeStatesUri"
        Write-Host "WorkItemTypeRulesFilePath :$global:WorkItemTypeRulesFilePath"
        Write-Host "WorkItemTypeRulesUri :$global:WorkItemTypeRulesUri"
        Write-Host "CreateWorkItemTypeControlUri :$global:CreateWorkItemTypeControlUri"
        Write-Host "GetRepositoryUri : $global:GetRepositoryUri"
        Write-Host "CreateTeamDashboardUrl : $global:CreateTeamDashboardUrl"
        Write-Host "GetPickListUri : $global:GetPickListUri"
        Write-Host "UpdateWorkItemTypeGroupUri :$global:UpdateWorkItemTypeGroupUri"
        Write-Host "UpdateWorkItemTypeControlUri :$global:UpdateWorkItemTypeControlUri"
        
        #RollBack
         Write-Host "DeleteProjectUri : $global:DeleteProjectUri"
         Write-Host "DeleteProcessUri : $global:DeleteProcessUri"
         Write-Host "DeleteFieldUri : $global:DeleteFieldUri"         
    }
}
